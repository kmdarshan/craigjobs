//
//  displayJobDetails.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface displayJobDetails : UITableViewController<UIAlertViewDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate>{
    NSString *position;
    NSString *description;
    NSString *url;
    NSString *city;
    NSString *postingid;
    NSString *tablename;
    IBOutlet UILabel *positionLabel;
    IBOutlet UITextView *descTextView;
    NSMutableArray *listOfItems;
    NSUInteger lenDesc;
    NSMutableData *responseData;
    UIBarButtonItem *forwardButton;
    UIActionSheet *sheet;
    NSString *load; // stores yes/no
    NSString *g_position;
    NSString *g_url;
    NSString *g_city;
    NSString *g_email;
    NSString *fromWhere;
    NSString *savedJobsPosition;
    NSString *g_reloadUrlString;
    NSString *defaultResumeString;
    //IBOutlet TDDatePickerController* datePickerView;
    //UILabel *activityLabel;
    //UIActivityIndicatorView *activityIndicator;
    //UIView *container;
    //CGRect frame;
}
-(id)initWithFrame:(CGRect) theFrame;
@property (nonatomic, retain) NSString *savedJobsPosition;
@property (nonatomic, retain) NSString *fromWhere;
@property (nonatomic, retain) UIBarButtonItem *forwardButton;
@property (nonatomic, retain) UIActionSheet *actionSheet;
@property (nonatomic, retain) NSString *load;
@property (nonatomic, retain) NSString *position;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *postingid;
@property (weak, nonatomic) IBOutlet UILabel *posLabel;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@property (nonatomic, retain) NSString *tablename;
@property (nonatomic, retain) NSMutableArray *listOfItems;
@property (nonatomic) int lenDesc;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, retain) NSString *g_position;
@property (nonatomic, retain) NSString *g_city;
@property (nonatomic, retain) NSString *g_url;
@property (nonatomic, retain) NSString *g_email;
@property (nonatomic, retain) NSString *g_reloadUrlString;
@property (nonatomic, strong) TWTweetComposeViewController *twitterController;
-(IBAction)clickDisplaySendOptions:(id)sender;
-(void) openInEmail;
-(void) openInBrowser;
-(void) applyForJobInEmail;
-(void) saveJobForLater;
- (void) insertMostViewedJobs:(NSString *)postingid:(NSString *)position:(NSString*)url:(NSString *)desc:(NSString *)email:(NSString *)city:(NSString *)compensation;
-(void) displayButtons;
- (void) loadDescriptionTableAgain:(NSString*)reloadUrlString;
@end
