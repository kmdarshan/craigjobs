//
//  dropboxViewcontroller.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DropboxSDK/DropboxSDK.h>
@interface dropboxViewcontroller : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    DBRestClient *restClient;
    NSMutableArray *filenameArray;
    NSArray *directoryContent;
    NSString *loaded;
}
@property (nonatomic, strong) NSMutableArray *filenameArray;
@property (nonatomic, strong) NSArray *directoryContent;
- (void)didPressUnLink;
-(void)didPressLink;
-(void)listFiles;
@property (nonatomic, strong) UITableView *myTableView;
@property (nonatomic, strong) NSString *loaded;
@end
