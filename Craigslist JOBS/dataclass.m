//
//  dataclass.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "dataclass.h"

//DataClass.m    
@implementation DataClass    
@synthesize str;   

static DataClass *instance =nil;    
+(DataClass *)getInstance    
{    
    @synchronized(self)    
    {    
        if(instance==nil)    
        {    
            
            instance= [DataClass new];    
        }    
    }    
    return instance;    
} 
@end
