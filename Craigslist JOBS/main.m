//
//  main.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "redflowerAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([redflowerAppDelegate class]));
    }
}
