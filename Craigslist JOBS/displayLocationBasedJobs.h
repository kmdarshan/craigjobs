//
//  displayLocationBasedJobs.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"
@interface displayLocationBasedJobs : UITableViewController<CLLocationManagerDelegate,UIAlertViewDelegate, MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    NSString *dept;
    long long expectedLength;
	long long currentLength;

    NSMutableData *responseData;
    
    NSMutableArray *listOfPositions;
    NSMutableArray *listOfDates;
    NSMutableArray *listOfUrls;
    NSMutableArray *listCounties;
    NSMutableArray *listPostingIds;
    NSString *desctablename;    
    //UILabel *activityLabel;
    //UIActivityIndicatorView *activityIndicator;
    //UIView *container;
    //CGRect frame;
    NSString *tmpLoaded;
    NSString *tmpErrorShown;
}
@property (nonatomic, retain) NSString *tmpErrorShown;
-(id)initWithFrame:(CGRect) theFrame;
@property (nonatomic, retain) NSString *tmpLoaded;
@property (nonatomic, retain) NSString *dept;
@property (nonatomic, strong) CLLocationManager *myLocationManager;
@property (nonatomic, strong) CLGeocoder *myGeocoder;
@property (nonatomic, strong) CLLocation *myLocation;
@property (nonatomic, retain) NSMutableData *responseData;

@property (nonatomic, retain) NSMutableArray *listOfPositions;
@property (nonatomic, retain) NSMutableArray *listOfDates;
@property (nonatomic, retain) NSMutableArray *listOfUrls;
@property (nonatomic, retain) NSMutableArray *listCounties;
@property (nonatomic, retain) NSMutableArray *listPostingIds;
@property (nonatomic, retain) NSString *desctablename;
@end
