//
//  dropboxSelectResumes.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "dropboxSelectResumes.h"
@implementation dropboxSelectResumes
@synthesize filenameArray;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (DBRestClient *)restClient {
    if (!restClient) {
        restClient =
        [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}
// Add new method
- (void)dropboxAllFilesDownloaded:(NSNotification *)notif {
    
    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = NO;
    [self.tableView reloadData];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = YES;
    
    filenameArray = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dropboxAllFilesDownloaded:) name:@"com.redflower.dropbox.clicklogin" object:nil];
    
    [self didPressLink];
    [[self restClient] loadMetadata:@"/"];
    //NSLog(@"--%d",[filenameArray count]);
    
    self.tableView =
    [[UITableView alloc] initWithFrame:self.view.bounds
                                 style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    
    //[self.view addSubview:self.tableView];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self downloadResume:cell.textLabel.text:indexPath.row];
}

- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section{
    return [filenameArray count];
}


- (NSString *) tableView:(UITableView *)tableView
 titleForHeaderInSection:(NSInteger)section{
    NSString *result = nil;
    result = @"Resumes available in Dropbox";
    return result;
}

- (NSString *) tableView:(UITableView *)tableView
 titleForFooterInSection:(NSInteger)section{
    NSString *result = nil;
    return result;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}

- (CGFloat) tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    return 30.0f;
}

- (CGFloat) tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section{
    
    CGFloat result = 0.0f;
    //    if (section == 0){
    //      result = 30.0f;
    // }
    return result;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"com.redflower.dropbox.clicklogin" object:nil];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata {
    if (metadata.isDirectory) {
        NSLog(@"Folder '%@' contains:", metadata.path);
        for (DBMetadata *file in metadata.contents) {
            [filenameArray addObject:file.filename];
            //NSLog(@"\t%@", file.filename);
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.redflower.dropbox.clicklogin" object:filenameArray];
    }
}

- (void)restClient:(DBRestClient *)client
loadMetadataFailedWithError:(NSError *)error {
    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = NO;

    UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Failed to load resume.\nReasons: \n1) This is the first time, your linking this application to dropbox. \nHow to fix it:\n1) Please can you go back and click the download button again.\nPlease email support@craigslistjobsin.us, if this issue is not fixed."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [connectFailMessage show];
    NSLog(@"Error loading metadata: %@", error);
}

- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)localPath {
    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = NO;

    NSLog(@"File loaded into path: %@", localPath);
    UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Successfully downloaded resume to your phone."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [connectFailMessage show];

}

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error {
    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = NO;

    NSLog(@"There was an error loading the file - %@", error);
    UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Failed to download resume to your phone. Try again!!!"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [connectFailMessage show];
}
- (void)didPressLink {
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] link];
    }
}

- (void)didPressUnLink {
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] unlinkAll];
    }
}

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata {
    
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error {
    NSLog(@"File upload failed with error - %@", error);
}

-(void)downloadResume:(NSString*)filename:(NSInteger) row
{
    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = YES;

    //NSLog(@"%@", [filenameArray objectAtIndex:row]);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[filenameArray objectAtIndex:row]];
    if ([[filePath pathExtension] isEqualToString:@"txt"] || [[filePath pathExtension] isEqualToString:@"doc"]
        || [[filePath pathExtension] isEqualToString:@"pdf"] || [[filePath pathExtension] isEqualToString:@"docx"]){
        
        [[self restClient] loadFile:[@"/" stringByAppendingString:[filenameArray objectAtIndex:row]] intoPath:filePath];
    }else
    {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Sorry. Currently we only allow downloads of file types: PDF, WORD, DOC, TEXT formats. The files to be downloaded, should be in the root directory. If you need additional formats please send an email to support@craigslistjobsin.us. We will add it to the supported formats. Thank you."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *result = nil;
    static NSString *CellIdentifier = @"Cell";
    result = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (result == nil){
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                        reuseIdentifier:CellIdentifier];
    }
    result.accessoryType = UITableViewCellAccessoryNone;
    [result setSelectionStyle:UITableViewCellSelectionStyleGray];
    
        if([filenameArray count] >= indexPath.row && [filenameArray count] > 0)
        {
            //Create the button and add it to the cell
            /*
            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [button addTarget:self 
                       action:@selector(downloadResume:)forControlEvents:UIControlEventTouchDown];
            
            [button addTarget:self 
                       action:@selector(downloadResume:)forControlEvents:UIControlEventTouchDown];
            [button setTitle:@"Use ME" forState:UIControlStateNormal];
            button.frame = CGRectMake(230.0f, 5.0f, 70.0f, 30.0f);
            [result addSubview:button];
             */
            result.detailTextLabel.text = @"Click, to download file to phone";
            result.textLabel.text = [filenameArray objectAtIndex:indexPath.row];
            result.textLabel.font = [UIFont fontWithName:@"ArialMT" size:16];
            result.detailTextLabel.font = [UIFont fontWithName:@"ArialMT" size:12];
        }
        else
            result.textLabel.text = @"..";
    return result;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
@end
