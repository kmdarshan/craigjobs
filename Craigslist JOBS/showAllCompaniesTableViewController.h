//
//  showAllCompaniesTableViewController.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface showAllCompaniesTableViewController : UITableViewController
{
    NSMutableArray *listOfItems;
    NSMutableArray *listOfAlphabets;
    NSMutableDictionary *dictCompanies;
    BOOL searching;
}
@property (nonatomic, retain) NSMutableArray *listOfItems;
@property (nonatomic, retain) NSMutableArray *listOfAlphabets;
@property (nonatomic, retain) NSMutableDictionary *dictCompanies;
@end
