//
//  sendEmail.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface sendEmail : UIViewController<MFMailComposeViewControllerDelegate> 
{
    //IBOutlet UILabel *message;
}

//@property (nonatomic, retain) IBOutlet UILabel *message;
-(void)displayDetailedJobDescription;
//-(IBAction)showPicker:(id)sender;
-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;

@end
