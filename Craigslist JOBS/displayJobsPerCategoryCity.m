//
//  displayJobsPerCategoryCity.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "displayJobsPerCategoryCity.h"
#import "JSON.h"
#import "helperMethods.h"
#import "displayJobDetails.h"
@implementation displayJobsPerCategoryCity
@synthesize jobCategory=jobCategory_;
@synthesize previousCity=previousCity_;
@synthesize listOfPositions = listOfPositions;
@synthesize listOfDates = listOfDates;
@synthesize listOfUrls = listOfUrls;
@synthesize listCounties = listCounties;
@synthesize bShowDates = bShowDates_;
@synthesize listPostingIds = listPostingIds_;
@synthesize desctablename = desctablename_;
@synthesize responseData;
@synthesize tmpErrorShown;
@synthesize previousCompany=previousCompany_;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void) btnClicked:(id)sender
{
    //NSLog(@"hi");  
    if(bShowDates)
    {
        anotherButton.title = @"Show Dates";
        bShowDates=FALSE;
    }
    else
    {
        anotherButton.title = @"Show County";
        bShowDates=TRUE;
    }
    [self.tableView reloadData];
}

- (void)refresh:(id)sender
{
    if(self.tabBarController.selectedIndex == 7)
    {
        //NSLog(@"companies");
        NSString *tmpJobCategory = self.jobCategory;
        NSString *tmpCompany = self.previousCompany;
        helperMethods *myInstance = [[helperMethods alloc] init];
        NSString *tmpDescTableName = [myInstance returnDatabaseTableDescName:tmpJobCategory];
        tmpCompany = [@"?company=" stringByAppendingFormat:tmpCompany];
        // set the table name of the description
        desctablename = tmpDescTableName;
        tmpJobCategory = [@"&tablename=" stringByAppendingFormat:tmpDescTableName];
        
        
        //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/getJobsFromCompanies.php";
        NSString *urlAsString = @"http://69.194.193.163/cgj/getJobsFromCompanies.php";
        urlAsString = [urlAsString stringByAppendingString:tmpCompany];
        urlAsString = [urlAsString stringByAppendingString:tmpJobCategory];
        //NSLog(urlAsString);
        
        // Do any additional setup after loading the view, typically from a nib.
        // Create the request.
        NSURLRequest *theRequest = 
        [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                         cachePolicy:NSURLRequestReloadRevalidatingCacheData
                     timeoutInterval:10.0];
        
        // create the connection with the request
        // and start loading the data
        // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            
            self.responseData = [NSMutableData data];
            
        } else {
            
            // Inform the user that the connection failed.
            
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        }    
    }else

    if(self.tabBarController.selectedIndex == 0)
    {
    tmpErrorShown = @"";
    NSString *tmpSelectedCity = self.previousCity;
    // replace the city with the shortcut
    // san francisco -> sfbay
    tmpSelectedCity = [tmpSelectedCity lowercaseString];
    NSString *searchString = @"francisco";
    NSRange prefixRange = [tmpSelectedCity rangeOfString:searchString
                                                 options:(NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpSelectedCity = @"sfbay";
    }
    
    // remove all spaces
    // useful for searching the urls instead of city
    tmpSelectedCity = [tmpSelectedCity stringByReplacingOccurrencesOfString:@" " withString:@""];
    tmpSelectedCity = [@"?city=" stringByAppendingFormat:tmpSelectedCity];
    NSString *tmpJobCategory = self.jobCategory;
    
    helperMethods *myInstance = [[helperMethods alloc] init];
    NSString *tmpDescTableName = [myInstance returnDatabaseTableDescName:tmpJobCategory];
    
    // set the table name of the description
    desctablename = tmpDescTableName;
    tmpJobCategory = [@"&tablename=" stringByAppendingFormat:tmpDescTableName];
    
    
    //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/getJobsFromCityAndJobType.php";
    NSString *urlAsString = @"http://69.194.193.163/cgj/getJobsFromCityAndJobType.php";
    urlAsString = [urlAsString stringByAppendingString:tmpSelectedCity];
    urlAsString = [urlAsString stringByAppendingString:tmpJobCategory];
    //NSLog(urlAsString);
    // Do any additional setup after loading the view, typically from a nib.
    // Create the request.
    NSURLRequest *theRequest = 
    [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                     cachePolicy:NSURLRequestReloadRevalidatingCacheData
                 timeoutInterval:10.0];
    
    // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if (theConnection) {
        
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        
        self.responseData = [NSMutableData data];
        
    } else {
        
        // Inform the user that the connection failed.
        
		UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Connection " message:@"Failed to load data"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[connectFailMessage show];
        
    }
    }
    else
    {
        NSString *urlAsString = @"http://69.194.193.163/cgj/getMostViewed.php";
        
        // Do any additional setup after loading the view, typically from a nib.
        // Create the request.
        NSURLRequest *theRequest = 
        [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                         cachePolicy:NSURLRequestReloadRevalidatingCacheData
                     timeoutInterval:30.0];
        
        // create the connection with the request
        // and start loading the data
        // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            
            self.responseData = [NSMutableData data];
            
        } else {
            
            // Inform the user that the connection failed.
            
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        }
    }
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:64/256.0 green:191/256.0 blue:23/256.0 alpha:1.0];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 133.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.translucent = self.navigationController.navigationBar.translucent;
    tools.barStyle = self.navigationController.navigationBar.barStyle;  
    tools.backgroundColor = self.navigationController.navigationBar.backgroundColor;
    tools.tintColor = self.navigationController.navigationBar.tintColor;
    //tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    // anyone know how to get it perfect?
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Create a standard refresh button.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc]
                           initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
    [buttons addObject:bi];
    
    // Create a spacer.
    bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    bi.width = 3.0f;
    [buttons addObject:bi];
    
    // Add profile button.
    anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Show Dates" style:UIBarButtonItemStylePlain target:self action:@selector(btnClicked:)];
    anotherButton.style = UIBarButtonItemStyleBordered;
    [buttons addObject:anotherButton];
    
    // Add buttons to toolbar and toolbar to nav bar.
    [tools setItems:buttons animated:NO];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    
    //anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Show Dates" style:UIBarButtonItemStylePlain target:self action:@selector(btnClicked:)];
    //self.navigationItem.rightBarButtonItem = anotherButton;
    bShowDates=FALSE;
    if(self.tabBarController.selectedIndex == 7)
    {
        //NSLog(@"companies");
        NSString *tmpJobCategory = self.jobCategory;
        NSString *tmpCompany = self.previousCompany;
        helperMethods *myInstance = [[helperMethods alloc] init];
        NSString *tmpDescTableName = [myInstance returnDatabaseTableDescName:tmpJobCategory];
        tmpCompany = [@"?company=" stringByAppendingFormat:tmpCompany];
        // set the table name of the description
        desctablename = tmpDescTableName;
        tmpJobCategory = [@"&tablename=" stringByAppendingFormat:tmpDescTableName];
        
        
        //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/getJobsFromCompanies.php";
        NSString *urlAsString = @"http://69.194.193.163/cgj/getJobsFromCompanies.php";
        urlAsString = [urlAsString stringByAppendingString:tmpCompany];
        urlAsString = [urlAsString stringByAppendingString:tmpJobCategory];
        //NSLog(urlAsString);
        
        // Do any additional setup after loading the view, typically from a nib.
        // Create the request.
        NSURLRequest *theRequest = 
        [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                         cachePolicy:NSURLRequestReloadRevalidatingCacheData
                     timeoutInterval:10.0];
        
        // create the connection with the request
        // and start loading the data
        // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            
            self.responseData = [NSMutableData data];
            
        } else {
            
            // Inform the user that the connection failed.
            
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        }    
    }else
    
    if (self.tabBarController.selectedIndex == 0)
    {
        NSString *tmpSelectedCity = self.previousCity;
        // replace the city with the shortcut
        // san francisco -> sfbay
        tmpSelectedCity = [tmpSelectedCity lowercaseString];
        NSString *searchString = @"francisco";
        NSRange prefixRange = [tmpSelectedCity rangeOfString:searchString
                                                     options:(NSCaseInsensitiveSearch)];
        if(prefixRange.length > 0)
        {
            tmpSelectedCity = @"sfbay";
        }
        
        // remove all spaces
        // useful for searching the urls instead of city
        tmpSelectedCity = [tmpSelectedCity stringByReplacingOccurrencesOfString:@" " withString:@""];
        tmpSelectedCity = [@"?city=" stringByAppendingFormat:tmpSelectedCity];
        NSString *tmpJobCategory = self.jobCategory;
        
        helperMethods *myInstance = [[helperMethods alloc] init];
        NSString *tmpDescTableName = [myInstance returnDatabaseTableDescName:tmpJobCategory];
        
        // set the table name of the description
        desctablename = tmpDescTableName;
        tmpJobCategory = [@"&tablename=" stringByAppendingFormat:tmpDescTableName];
        
        
        //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/getJobsFromCityAndJobType.php";
        NSString *urlAsString = @"http://69.194.193.163/cgj/getJobsFromCityAndJobType.php";
        urlAsString = [urlAsString stringByAppendingString:tmpSelectedCity];
        urlAsString = [urlAsString stringByAppendingString:tmpJobCategory];
        //NSLog(urlAsString);
        //
        // Do any additional setup after loading the view, typically from a nib.
        // Create the request.
        NSURLRequest *theRequest = 
        [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                         cachePolicy:NSURLRequestReloadRevalidatingCacheData
                     timeoutInterval:10.0];
        
        // create the connection with the request
        // and start loading the data
        // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            
            self.responseData = [NSMutableData data];
            
        } else {
            
            // Inform the user that the connection failed.
            
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        }
    }else{
        NSString *urlAsString = @"http://69.194.193.163/cgj/getMostViewed.php";
        
        // Do any additional setup after loading the view, typically from a nib.
        // Create the request.
        NSURLRequest *theRequest = 
        [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                         cachePolicy:NSURLRequestReloadRevalidatingCacheData
                     timeoutInterval:10.0];
        
        // create the connection with the request
        // and start loading the data
        // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            
            self.responseData = [NSMutableData data];
            
        } else {
            
            // Inform the user that the connection failed.
            
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        }

    }
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
    expectedLength = [response expectedContentLength];
	currentLength = 0;
	HUD.mode = MBProgressHUDModeDeterminate;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    //NSLog(@"receive data");
    if ([data length] > 0){
        //NSLog(@"%lu bytes of data was returned.", (unsigned long)[data length]);
        
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Error receiving data from server due to internet connectivity. Please check back again."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
	[responseData appendData:data];
    currentLength += [data length];
	HUD.progress = currentLength / (float)expectedLength;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //[HUD hide:YES];
	NSString *errorMessage = [NSString stringWithFormat:@"Connection failed: %@", [error description]];
    NSLog(@"connection failed %@", errorMessage);
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"Sorry.No Internet Connection/Request timed out. Please try again."
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
    [alertView show];
    [HUD hide:YES afterDelay:1];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
        //NSLog(@"response val");
    NSString *responseString = [[NSString alloc] initWithData:responseData
                                                     encoding:NSUTF8StringEncoding];
    //NSLog(responseString);
    NSArray *userData = [responseString JSONValue];
    int elements = [userData count];
    if (elements < 1) {
        if([tmpErrorShown length] < 1)
        {
            tmpErrorShown = @"error";

        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"No jobs opening available at this moment. Please check back again or try refreshing/reloading the page."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
        }
    }else
    {
        NSMutableArray *responsePositions = [NSMutableArray array];
        NSMutableArray *responseDate = [NSMutableArray array];
        NSMutableArray *responseUrls = [NSMutableArray array];
        NSMutableArray *responseCounties = [NSMutableArray array];
        NSMutableArray *responsePostingIds = [NSMutableArray array];
        
        for (NSDictionary *user in userData) {
            [responsePositions addObject:[user objectForKey:@"position"]];
            [responseDate addObject:[user objectForKey:@"date"]];
            [responseUrls addObject:[user objectForKey:@"url"]];
            [responseCounties addObject:[user objectForKey:@"city"]];
            [responsePostingIds addObject:[user objectForKey:@"posting_id"]];
            
        }
        self.listOfPositions = responsePositions;
        self.listOfDates = responseDate;
        self.listOfUrls = responseUrls;
        self.listCounties = responseCounties;
        self.listPostingIds = responsePostingIds;
        
        [self.tableView reloadData];
    }
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    HUD.mode = MBProgressHUDModeCustomView;
	[HUD hide:YES afterDelay:1];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return [self.listOfPositions count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSLog(@"hi");
    UITableViewCell *result = nil;
    static NSString *TableViewCellIdentifier = @"MyCells";
    result = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (result == nil){
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableViewCellIdentifier];
    }

    //NSLog(@"index path %d", indexPath.row);
    //NSLog(@"count %d ", [self.listOfPositions count]);
    if ([self.listOfPositions objectAtIndex:indexPath.row] != (id)[NSNull null]) {
        //NSLog(@"not nul");
        result.textLabel.text = [self.listOfPositions objectAtIndex:indexPath.row];
        if(bShowDates == FALSE)
            result.detailTextLabel.text = [self.listCounties objectAtIndex:indexPath.row];
        else
            result.detailTextLabel.text = [self.listOfDates objectAtIndex:indexPath.row];
        
        result.detailTextLabel.font = [UIFont fontWithName:@"ArialMT" size:10];
        result.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        result.textLabel.font = [UIFont fontWithName:@"ArialMT" size:13];
    }
    return result;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    if (self.tabBarController.selectedIndex ==4)
    {
        NSString *tmpPosition = [self.listOfPositions objectAtIndex:indexPath.row];
        //NSString *tmpPostingIds = [self.listPostingIds objectAtIndex:indexPath.row];
        NSString *tmpNumberPostingIds = [self.listPostingIds objectAtIndex:indexPath.row];
        displayJobDetails *displayCitiesController = [displayJobDetails alloc];
        displayCitiesController.position = tmpPosition; 
        displayCitiesController.postingid = tmpNumberPostingIds;
        displayCitiesController.description = @"";
        displayCitiesController.load = @"yes";
        displayCitiesController.fromWhere = @"mostviewed";
        //NSString *tmpVal = [self.listOfUrls objectAtIndex:indexPath.row];
        displayCitiesController.url = [self.listOfUrls objectAtIndex:indexPath.row];
        displayCitiesController.city = [self.listCounties objectAtIndex:indexPath.row];
        displayCitiesController.tablename = desctablename;
        [self.navigationController pushViewController:displayCitiesController animated:YES];
    }else{
        NSString *tmpPosition = [self.listOfPositions objectAtIndex:indexPath.row];
        //NSString *tmpPostingIds = [self.listPostingIds objectAtIndex:indexPath.row];
        NSString *tmpNumberPostingIds = [self.listPostingIds objectAtIndex:indexPath.row];
        displayJobDetails *displayCitiesController = [displayJobDetails alloc];
        displayCitiesController.position = tmpPosition; 
        displayCitiesController.postingid = tmpNumberPostingIds;
        displayCitiesController.description = @"";
        displayCitiesController.load = @"yes";
        //NSString *tmpVal = [self.listOfUrls objectAtIndex:indexPath.row];
        displayCitiesController.url = [self.listOfUrls objectAtIndex:indexPath.row];
        displayCitiesController.city = [self.listCounties objectAtIndex:indexPath.row];
        displayCitiesController.tablename = desctablename;
        [self.navigationController pushViewController:displayCitiesController animated:YES];   
    }
}

@end