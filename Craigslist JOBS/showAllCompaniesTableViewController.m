//
//  showAllCompaniesTableViewController.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "showAllCompaniesTableViewController.h"
#import "displayJobCategoriesTableView.h"
@implementation showAllCompaniesTableViewController
@synthesize listOfItems;
@synthesize listOfAlphabets;
@synthesize dictCompanies;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if([listOfAlphabets count]==0)
        return @"";
    return [listOfAlphabets objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	
	if(searching)
		return nil;
    
	return listOfAlphabets;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
	if(searching)
		return -1;
	
	NSInteger count = 0;
    for(NSString *character in listOfAlphabets)
    {
        if([character isEqualToString:title])
            return count;
        count ++;
    }
    return 0;// in case of some eror donot crash d application
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    searching = FALSE;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    dictCompanies = [[NSMutableDictionary alloc] init];
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath   = [mainBundle pathForResource:@"companies" ofType:@"plist"];
    listOfItems = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    
    listOfAlphabets = [[NSMutableArray alloc] init];
	[listOfAlphabets addObject:@"A"];
	[listOfAlphabets addObject:@"B"];
	[listOfAlphabets addObject:@"C"];
	[listOfAlphabets addObject:@"D"];
	[listOfAlphabets addObject:@"E"];
	[listOfAlphabets addObject:@"F"];
	[listOfAlphabets addObject:@"G"];
	[listOfAlphabets addObject:@"H"];
	[listOfAlphabets addObject:@"I"];
	[listOfAlphabets addObject:@"J"];
	[listOfAlphabets addObject:@"K"];
	[listOfAlphabets addObject:@"L"];
	[listOfAlphabets addObject:@"M"];
	[listOfAlphabets addObject:@"N"];
	[listOfAlphabets addObject:@"O"];
	[listOfAlphabets addObject:@"P"];
	[listOfAlphabets addObject:@"Q"];
	[listOfAlphabets addObject:@"R"];
	[listOfAlphabets addObject:@"S"];
	[listOfAlphabets addObject:@"T"];
	[listOfAlphabets addObject:@"U"];
	[listOfAlphabets addObject:@"V"];
	[listOfAlphabets addObject:@"W"];
	[listOfAlphabets addObject:@"X"];
	[listOfAlphabets addObject:@"Y"];
	[listOfAlphabets addObject:@"Z"];
    
    for (NSString *c in listOfAlphabets) {
        NSMutableArray *arrayOfNames = [[NSMutableArray alloc]init];
        for (NSString *tmpcomp in listOfItems) {
            char tmpchar = [tmpcomp characterAtIndex:0];
            NSString *tmpstr = [NSString stringWithFormat:@"%c", tmpchar];
            tmpstr = [tmpstr capitalizedString];
            
            if([tmpstr isEqualToString:c])
            {
                
                //NSLog(@"adding %@",tmpcomp);
                [arrayOfNames addObject:tmpcomp];
                //[dictCompanies setObject:arrayOfNames forKey:[NSString stringWithFormat:@"%s",c]];
            }
        }
        //NSLog(@"=> %d", [arrayOfNames count]);
        //NSLog(c);
        [dictCompanies setObject:arrayOfNames forKey:c];
        //NSLog(@"XXX %d", [dictCompanies count]);
    }
    /*
     for( NSString *aKey in [dictCompanies allKeys] )
    {
        // do something
        NSLog(@"..");
        NSLog(aKey);
    }*/    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [listOfAlphabets count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[dictCompanies objectForKey:[listOfAlphabets objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"tmp_cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:MyIdentifier];
    }
    
    cell.textLabel.text = [[dictCompanies objectForKey:[listOfAlphabets objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    NSString *tmpString = [[dictCompanies objectForKey:[listOfAlphabets objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    displayJobCategoriesTableView *displayCitiesController = [displayJobCategoriesTableView alloc];
    displayCitiesController.selectedCompany = tmpString;
    //NSString *val = [[dictCompanies objectForKey:[listOfAlphabets objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    //NSLog(@"selected index => %@", val);
    //NSLog(@"selected section => %d", indexPath.section);
    [self.navigationController pushViewController:displayCitiesController animated:YES];
}

@end
