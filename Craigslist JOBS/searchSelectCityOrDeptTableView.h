//
//  searchSelectCityOrDeptTableView.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface searchSelectCityOrDeptTableView : UITableViewController{
    
    NSMutableArray *listHolder;
    NSString *selectedid;
}
@property (nonatomic, retain) NSMutableArray *listHolder;
@property (nonatomic, retain) NSString *selectedid;
@end
