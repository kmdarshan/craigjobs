//
//  displayLocationBasedJobs.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "displayLocationBasedJobs.h"
#import "displayJobDetails.h"
#import "helperMethods.h"
#import "JSON.h"
@implementation displayLocationBasedJobs
@synthesize myLocationManager;
@synthesize myGeocoder;
@synthesize myLocation;
@synthesize responseData;
@synthesize dept=dept_;
@synthesize desctablename;
@synthesize tmpLoaded;
@synthesize listOfPositions = listOfPositions;
@synthesize listOfDates = listOfDates;
@synthesize listOfUrls = listOfUrls;
@synthesize listCounties = listCounties;
@synthesize listPostingIds = listPostingIds;
@synthesize tmpErrorShown;
-(id)initWithFrame:(CGRect)theFrame {
    if (self = [super init]) {
        //frame = theFrame;
        //self.view.frame = theFrame;
    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    
    self.myLocation = newLocation;
    /* We received the new location */
    // NSLog(@"Latitude = %f", newLocation.coordinate.latitude);
    // NSLog(@"Longitude = %f", newLocation.coordinate.longitude);
    
    self.myGeocoder = [[CLGeocoder alloc] init];
    [self.myGeocoder
     reverseGeocodeLocation:myLocation
     completionHandler:^(NSArray *placemarks, NSError *error) {
         if (error == nil &&
             [placemarks count] > 0){
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             /* We received the results */
             //NSLog(@"Country = %@", placemark.country);
             //NSLog(@"Postal Code = %@", placemark.postalCode);
             //NSLog(@"Locality = %@", placemark.locality);
             NSString *tmpLocality = placemark.locality;
             
             [self.myLocationManager stopUpdatingLocation];
             
             //NSString *tmpSelectedCity = [@"?city=" stringByAppendingFormat:tmpLocality];
             NSString *tmpSelectedCity = [@"?city=" stringByAppendingFormat:tmpLocality];
             NSString *tmpJobCategory = self.dept;
             
             helperMethods *myInstance = [[helperMethods alloc] init];
             NSString *tmpDescTableName = [myInstance returnDatabaseTableDescName:self.dept];
             desctablename = tmpDescTableName;

             // set the table name of the description
             tmpJobCategory = [@"&tablename=" stringByAppendingFormat:tmpDescTableName];
             
             
             //NSString *urlAsString = @"http://192.168.1.9/~spycar/cgjobs/jobsNearMe.php";
             NSString *urlAsString = @"http://69.194.193.163/cgj/jobsNearMe.php";
             urlAsString = [urlAsString stringByAppendingString:tmpSelectedCity];
             urlAsString = [urlAsString stringByAppendingString:tmpJobCategory];
             NSString *tmpFormatedURL = [urlAsString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
             //NSLog(@"%@", tmpFormatedURL);
            
             // Do any additional setup after loading the view, typically from a nib.
             // Create the request.
             NSURLRequest *theRequest = 
             [NSURLRequest requestWithURL:[NSURL URLWithString:tmpFormatedURL]
                              cachePolicy:NSURLRequestReloadRevalidatingCacheData
                          timeoutInterval:30.0];
             
             NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
             
             if (theConnection) {
                 self.responseData = [NSMutableData data];

                 
             } else {
                 
                 // Inform the user that the connection failed.
                 
                 UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"URLConnection " message:@"Failed in getting connection"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                 [connectFailMessage show];
                 [HUD hide:YES afterDelay:1];
                 
             }

         }
         else if (error == nil &&
                  [placemarks count] == 0){
             NSLog(@"No results were returned.");
             [HUD hide:YES afterDelay:1];
         }
         else if (error != nil){
             NSLog(@"An error occurred = %@", error);
             [HUD hide:YES afterDelay:1];
         }
     }];
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information " message:@"Application does not have access to the location data. Goto settings, location services and enable locations for this application. Restart application after you have done that."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [connectFailMessage show];
    //NSLog(@"An error occurred 1 = %@", error);
    [HUD hide:YES afterDelay:1];
    /* Failed to receive user's location */
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:19/256.0 green:133/256.0 blue:45/256.0 alpha:1.0];
    /*container = [[UIView alloc] initWithFrame:CGRectMake(150, 200, 110, 100)];
    activityLabel = [[UILabel alloc] init];
    activityLabel.text = NSLocalizedString(@"LOADING", @"string1");
    activityLabel.textColor = [UIColor blackColor];
    activityLabel.font = [UIFont boldSystemFontOfSize:14];
    [container addSubview:activityLabel];
    
    activityLabel.frame = CGRectMake(180, 200,100, 25);
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [container addSubview:activityIndicator];
    activityIndicator.frame = CGRectMake(130, 200, 70, 25);
    
    [self.view addSubview:container];
    container.center = CGPointMake(frame.size.width/2, frame.size.height/2);
    self.view.backgroundColor = [UIColor whiteColor];*/
    
    // Create a standard refresh button.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc]
                           initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
    self.navigationItem.rightBarButtonItem = bi;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if ([CLLocationManager locationServicesEnabled]){
        self.myLocationManager = [[CLLocationManager alloc] init];
        self.myLocationManager.delegate = self;
        self.myLocationManager.purpose =
        @"To provide functionality based on user's current location.";
        [self.myLocationManager startUpdatingLocation];
        
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
    } else {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Location services are not enabled on your phone."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        /* Location services are not enabled.
         Take appropriate action: for instance, prompt the
         user to enable the location services */
        //NSLog(@"Location services are not enabled");
    }
}

- (void) refresh:(id)sender
{
    //tmpErrorShown = @"";
    //activityLabel.hidden = FALSE;
    //[activityIndicator startAnimating];
    if ([CLLocationManager locationServicesEnabled]){
        self.myLocationManager = [[CLLocationManager alloc] init];
        self.myLocationManager.delegate = self;
        self.myLocationManager.purpose =
        @"To provide functionality based on user's current location.";
        [self.myLocationManager startUpdatingLocation];
        
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
    } else {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Location services are not enabled on your phone."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        /* Location services are not enabled.
         Take appropriate action: for instance, prompt the
         user to enable the location services */
        //NSLog(@"Location services are not enabled");
    }
 
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [self.myLocationManager stopUpdatingLocation];
    self.myLocationManager = nil;
    self.myGeocoder = nil;
    [HUD hide:YES afterDelay:1];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self setTitle:self.dept];
    if(self.tmpLoaded == @"false")
    {
        self.tmpLoaded = @"true";
        //[activityIndicator startAnimating];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [HUD hide:YES afterDelay:1];
    //activityLabel.hidden = true;
    //[activityIndicator stopAnimating];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return [self.listOfPositions count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSLog(@"hi");
    UITableViewCell *result = nil;
    static NSString *TableViewCellIdentifier = @"MyCells";
    result = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (result == nil){
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableViewCellIdentifier];
    }
    
    result.textLabel.text = [self.listOfPositions objectAtIndex:indexPath.row];
    result.detailTextLabel.text = [self.listOfDates objectAtIndex:indexPath.row];
    
    result.detailTextLabel.font = [UIFont fontWithName:@"ArialMT" size:10];
    result.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    result.textLabel.font = [UIFont fontWithName:@"ArialMT" size:13];
    return result;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *tmpPosition = [self.listOfPositions objectAtIndex:indexPath.row];
    NSString *tmpNumberPostingIds = [self.listPostingIds objectAtIndex:indexPath.row];
    //NSNumber *tmpNumber = [self.listPostingIds objectAtIndex:indexPath.row];
    //NSLog(tmpNumber);
    NSString *tmpUrl = [self.listOfUrls objectAtIndex:indexPath.row];
    displayJobDetails *displayCitiesController = [displayJobDetails alloc];
    displayCitiesController.position = tmpPosition; 
    displayCitiesController.postingid = tmpNumberPostingIds;
    displayCitiesController.description = @"";
    displayCitiesController.load = @"yes";
    //NSString *tmpVal = [self.listOfUrls objectAtIndex:indexPath.row];
    displayCitiesController.url = tmpUrl;
    displayCitiesController.city = [self.listCounties objectAtIndex:indexPath.row];
    displayCitiesController.tablename = desctablename;
    [self.navigationController pushViewController:displayCitiesController animated:YES];
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tmpPosition = [self.listOfPositions objectAtIndex:indexPath.row];
    NSString *tmpNumberPostingIds = [self.listPostingIds objectAtIndex:indexPath.row];
    //NSNumber *tmpNumber = [self.listPostingIds objectAtIndex:indexPath.row];
    //NSLog(tmpNumber);
    NSString *tmpUrl = [self.listOfUrls objectAtIndex:indexPath.row];
    displayJobDetails *displayCitiesController = [displayJobDetails alloc];
    displayCitiesController.position = tmpPosition; 
    displayCitiesController.postingid = tmpNumberPostingIds;
    displayCitiesController.description = @"";
    displayCitiesController.load = @"yes";
    //NSString *tmpVal = [self.listOfUrls objectAtIndex:indexPath.row];
    displayCitiesController.url = tmpUrl;
    displayCitiesController.city = [self.listCounties objectAtIndex:indexPath.row];
    displayCitiesController.tablename = desctablename;
    [self.navigationController pushViewController:displayCitiesController animated:YES];
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
    HUD.mode = MBProgressHUDModeDeterminate;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    //NSLog(@"receive data");
    if ([data length] > 0){
        //NSLog(@"%lu bytes of data was returned.", (unsigned long)[data length]);
        //NSLog(@"data recevied");
        
    }else
    {
        //activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Error receiving data from server due to internet connectivity. Please check back again."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
	[responseData appendData:data];
    currentLength += [data length];
	HUD.progress = currentLength / (float)expectedLength;

}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //[HUD hide:YES];
    //activityLabel.hidden = TRUE;
    //[activityIndicator stopAnimating];
    
	//NSString *errorMessage = [NSString stringWithFormat:@"Connection failed: %@", [error description]];
    //NSLog(@"connection failed %@", errorMessage);
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"Sorry.No Internet Connection/Request timed out. Please try again."
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
    [alertView show];
    [HUD hide:YES afterDelay:1];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    //NSLog(@"response val");
    //NSLog(responseData);
    NSString *responseString = [[NSString alloc] initWithData:responseData
                                                     encoding:NSUTF8StringEncoding];
    //NSLog(responseString);
    NSArray *userData = [responseString JSONValue];
    int elements = [userData count];
    if (elements < 1) {
        
        //NSLog(tmpErrorShown);
        //activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];
        if([tmpErrorShown length] < 1)
        {
            tmpErrorShown = @"error";
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Sorry, we could not get the jobs near you. Try refreshing the page or reloading the page again. This might help in retrieving the jobs faster. Or please check back again."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
        }
    }else
    {
        //activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];
        tmpErrorShown = @"dontshowerror";
        NSMutableArray *responsePositions = [NSMutableArray array];
        NSMutableArray *responseDate = [NSMutableArray array];
        NSMutableArray *responseUrls = [NSMutableArray array];
        NSMutableArray *responseCounties = [NSMutableArray array];
        NSMutableArray *responsePostingIds = [NSMutableArray array];
        
        for (NSDictionary *user in userData) {
            [responsePositions addObject:[user objectForKey:@"position"]];
            [responseDate addObject:[user objectForKey:@"date"]];
            [responseUrls addObject:[user objectForKey:@"url"]];
            [responseCounties addObject:[user objectForKey:@"city"]];
            NSString *tmpStrPostingIDname;
            tmpStrPostingIDname = [NSString stringWithFormat:@"%@", [user valueForKey:@"posting_id"]];
            //NSLog(tmpStrPostingIDname);
            //[responsePostingIds addObject:[user objectForKey:@"posting_id"]];
            [responsePostingIds addObject:tmpStrPostingIDname];
            
        }
        self.listOfPositions = responsePositions;
        self.listOfDates = responseDate;
        self.listOfUrls = responseUrls;
        self.listCounties = responseCounties;
        self.listPostingIds = responsePostingIds;
        
        [self.tableView reloadData];
    }
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    HUD.mode = MBProgressHUDModeCustomView;
	[HUD hide:YES afterDelay:1];
}

@end
