//
//  localReminderViewViewController.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 6/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "localReminderViewViewController.h"

@interface localReminderViewViewController ()

@end

@implementation localReminderViewViewController
@synthesize datePicker;
@synthesize position;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
	self.title = @"Add Reminder";
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAddNotification)];
	self.navigationItem.leftBarButtonItem = cancelButton;
    
	UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(addNotification)];
	self.navigationItem.rightBarButtonItem = addButton;
	
	//set the date pickers date and the minimum date to now
	[self.datePicker setDate:[NSDate date]];
	[self.datePicker setMinimumDate:[NSDate date]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)cancelAddNotification {
	[self dismissModalViewControllerAnimated:YES];
}

- (void)addNotification {
	UILocalNotification *localNotification = [[UILocalNotification alloc] init];
	if (localNotification == nil)
        return;
	localNotification.fireDate = self.datePicker.date;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    NSString *tmpstr = @"You need to apply for this job: ";
    localNotification.alertBody = [tmpstr stringByAppendingString:self.position];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
	localNotification.applicationIconBadgeNumber = 1;
	localNotification.alertAction = @"Apply Now";
	NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:@"Object 1", @"Key 1", @"Object 2", @"Key 2", nil];
    localNotification.userInfo = infoDict;
	
	[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
	[self dismissModalViewControllerAnimated:YES];
}

@end
