//
//  dropboxSelectResumes.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DropboxSDK/DropboxSDK.h>
@interface dropboxSelectResumes : UITableViewController
{
    NSMutableArray *filenameArray;
    DBRestClient *restClient;
}
@property (nonatomic, strong) NSMutableArray *filenameArray;
- (void)didPressUnLink;
-(void)didPressLink;
-(void)downloadResume:(NSString*)filename:(NSInteger) row;
@end
