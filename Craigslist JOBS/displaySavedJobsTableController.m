//
//  displaySavedJobsTableController.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "displaySavedJobsTableController.h"
#import "helperMethods.h"
#import "displayJobDetails.h"
@implementation displaySavedJobsTableController
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    helperMethods *myInstance = [helperMethods alloc];
    //NSUInteger index = [self.tabBarController.viewControllers indexOfObjectIdenticalTo:];
    if (self.tabBarController.selectedIndex == 3) 
        savedjobs = [myInstance fetchCoreDataJobs:@"Recentjobs"];
    else
        savedjobs = [myInstance fetchCoreDataJobs:@"Savedjobs"];
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    helperMethods *myInstance = [helperMethods alloc];
    //NSUInteger index = [self.tabBarController.viewControllers indexOfObjectIdenticalTo:];
    if (self.tabBarController.selectedIndex == 3) 
        savedjobs = [myInstance fetchCoreDataJobs:@"Recentjobs"];
    else
        savedjobs = [myInstance fetchCoreDataJobs:@"Savedjobs"];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    helperMethods *myInstance = [[helperMethods alloc] init];
    if (self.tabBarController.selectedIndex == 3) 
        savedjobs = [myInstance fetchCoreDataJobs:@"Recentjobs"];
    else
        savedjobs = [myInstance fetchCoreDataJobs:@"Savedjobs"];
    int count = [savedjobs count];
    //NSLog(@"coutn %d", count);
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSManagedObject *info = [savedjobs objectAtIndex:indexPath.row];
    // Configure the cell...
    cell.textLabel.text = [info valueForKey:@"jobname"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
// bookmarks
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    displayJobDetails *displayCitiesController = [displayJobDetails alloc];
    displayCitiesController.fromWhere = @"saved"; 
    NSString *inStr = [NSString stringWithFormat:@"%d", indexPath.row];
    displayCitiesController.savedJobsPosition = inStr;
    [self.navigationController pushViewController:displayCitiesController animated:YES];
}

@end
