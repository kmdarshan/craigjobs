//
//  displaySendOptions.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface displaySendOptions : UIViewController
{
    NSString *state;
    NSString *capital;
    IBOutlet UILabel *stateLabel;
    IBOutlet UILabel *capitalLabel;
}
@property (nonatomic, retain)NSString *state, *capital;
@property (nonatomic, retain)IBOutlet UILabel *stateLabel, *capitalLabel;


@end
