//
//  searchResultsTableViewController.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface searchResultsTableViewController : UITableViewController<UIAlertViewDelegate, MBProgressHUDDelegate>{
    MBProgressHUD *HUD;
    long long expectedLength;
	long long currentLength;
    NSString *keywords;
    NSString *jobname;
    NSString *city;
    NSString *dept;
    NSMutableData *responseData;
    NSMutableArray *listOfPositions;
    NSMutableArray *listOfDates;
    NSMutableArray *listOfUrls;
    NSMutableArray *listCounties;
    NSMutableArray *listPostingIds;
    NSString *desctablename;
    BOOL bShowDates;
    //UILabel *activityLabel;
    //UIActivityIndicatorView *activityIndicator;
    UIView *container;
    CGRect frame;
    NSString *tmpLoaded;
    NSString *tmpErrorShown;
}
@property (nonatomic, retain) NSString *tmpErrorShown;
@property (nonatomic, retain) NSString *tmpLoaded;
-(id)initWithFrame:(CGRect) theFrame;
@property (nonatomic, retain) NSString *keywords;
@property (nonatomic, retain) NSString *jobname;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *dept;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSMutableArray *listOfPositions;
@property (nonatomic, retain) NSMutableArray *listOfDates;
@property (nonatomic, retain) NSMutableArray *listOfUrls;
@property (nonatomic, retain) NSMutableArray *listCounties;
@property (nonatomic, retain) NSMutableArray *listPostingIds;
@property (nonatomic) BOOL bShowDates;
@property (nonatomic, retain) NSString *desctablename;
@end
