//
//  dropboxViewcontroller.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "dropboxViewcontroller.h"
#import "dropboxSelectResumes.h"
#import "helperMethods.h"
@implementation dropboxViewcontroller
@synthesize myTableView;
@synthesize filenameArray;
@synthesize directoryContent;
@synthesize loaded;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self listFiles];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata {
    if (metadata.isDirectory) {
        NSLog(@"Folder '%@' contains:", metadata.path);
        for (DBMetadata *file in metadata.contents) {
            [filenameArray addObject:file.filename];
            NSLog(@"\t%@", file.filename);
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.redflower.dropbox.clicklogin" object:filenameArray];
    }
}

- (void)restClient:(DBRestClient *)client
loadMetadataFailedWithError:(NSError *)error {
    
    NSLog(@"Error loading metadata: %@", error);
}

- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)localPath {
    NSLog(@"File loaded into path: %@", localPath);
}

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error {
    NSLog(@"There was an error loading the file - %@", error);
}
- (DBRestClient *)restClient {
    if (!restClient) {
        restClient =
        [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}
- (void)didPressLink {
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] link];
    }
}

- (void)didPressUnLink {
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] unlinkAll];
    }
}

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata {
    
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error {
    NSLog(@"File upload failed with error - %@", error);
}

#pragma mark - View lifecycle

// Add new method
- (void)dropboxAllFilesDownloaded:(NSNotification *)notif {
    //[filenameArray removeAllObjects];
    // NSLog(@"table loaded");
    // NSLog(@" total files %d",[filenameArray count]);
    self.loaded = @"yes";
    [self.myTableView reloadData];
    //[self.myTableView beginUpdates];
    //NSIndexPath *temp = [[NSIndexPath alloc] initWithIndex:TEST];
    //NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow: 0 inSection: 3];
//    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow: 1 inSection: 3];
  //  NSArray *tmparr = [[NSArray alloc] initWithObjects:indexPath1, indexPath2,nil];
    //NSIndexPath *path = [[NSIndexPath alloc] initWithIndexes:indexPath length:1];
    //[self.tableView reloadRowsAtIndexPaths:tmparr withRowAnimation:UITableViewRowAnimationNone];
    //[self.tableView endUpdates];
    
}


- (void)viewDidLoad
{
    [self listFiles];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dropboxAllFilesDownloaded:) name:@"com.redflower.dropbox.clicklogin" object:nil];
    filenameArray = [[NSMutableArray alloc] init];
    [super viewDidLoad];
    self.myTableView =
    [[UITableView alloc] initWithFrame:self.view.bounds
                                 style:UITableViewStyleGrouped];
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    self.myTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    
    [self.view addSubview:self.myTableView];
    
    //[self didPressUnLink];
    //[self didPressLink];
    //NSString *localPath = [[NSBundle mainBundle] pathForResource:@"cities" ofType:@"plist"];
    //NSString *filename = @"cities.plist";
    //NSString *destDir = @"/";
    //[[self restClient] uploadFile:filename toPath:destDir withParentRev:nil fromPath:localPath];
    //[[self restClient] loadMetadata:@"/"];
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    //NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"SampleFile.txt"];
    //[[self restClient] loadFile:@"/cities.plist" intoPath:filePath];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section{
    if(section == 0)
        return 1;
    if(section == 1)
        return 1;
    if(section == 2)
        return [directoryContent count];
    if(section == 3)
        return [directoryContent count];
    if(section == 4)
        return [directoryContent count];
    return [directoryContent count];
}

- (NSString *) tableView:(UITableView *)tableView
 titleForHeaderInSection:(NSInteger)section{
    NSString *result = nil;
    if (section == 0){
        result = @"How to attach resumes:";
    }
    if (section == 1){
        result = @"Link Dropbox";
    }
    if (section == 2){
        result = @"Resumes on phone (select one)";
    }
    if (section == 3){
        result = @"Cover letters (select one)";
    }
    if (section == 4){
        result = @"Content/Body (select one)";
    }
    
    return result;
}
- (NSString *) tableView:(UITableView *)tableView
 titleForFooterInSection:(NSInteger)section{
    NSString *result = nil;
    return result;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        return 200.0f;
    }
    return 40.0f;
}

- (CGFloat) tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    CGFloat result = 0.0f;
    if (section == 0){
        //result = 30.0f;
    }
    if (section == 1){
        //result = 30.0f;
    }
    result = 30.0f;
    return result;
}

- (CGFloat) tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section{
    
    CGFloat result = 0.0f;
    //    if (section == 0){
    //      result = 30.0f;
    // }
    return result;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (void)viewDidUnload
{
    [self setMyTableView:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"com.redflower.dropbox.clicklogin" object:nil];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)downloadResume:(id)sender
{
    //Get the superview from this button which will be our cell
	UITableViewCell *owningCell = (UITableViewCell*)[sender superview];
	
	//From the cell get its index path.
	//
	//In this example I am only using the indexpath to show a unique message based
	//on the index of the cell.
	//If you were using an array of data to build the table you might use
	//the index here to do something with that array.
	NSIndexPath *pathToCell = [self.myTableView indexPathForCell:owningCell];
    //[[self restClient] loadMetadata:@"/"];
    NSLog(@"%@", [filenameArray objectAtIndex:pathToCell.row]);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[filenameArray objectAtIndex:pathToCell.row]];
    
    [[self restClient] loadFile:[@"/" stringByAppendingString:[filenameArray objectAtIndex:pathToCell.row]] intoPath:filePath];
}

- (void)listFiles {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSError *err;
	NSFileManager *fm = [NSFileManager defaultManager];
	directoryContent = [fm contentsOfDirectoryAtPath:documentsDirectory error:&err];
    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH 'pdf' OR self ENDSWITH '.txt' OR self ENDSWITH '.doc' OR self ENDSWITH '.docx'"];
    NSArray *onlyJPGs = [directoryContent filteredArrayUsingPredicate:fltr];
    directoryContent = [NSArray arrayWithArray:onlyJPGs];
    //NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(2, 3)];
    //[self.myTableView beginUpdates];
    //NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    //[indexSet addIndex:3];
    //[indexSet addIndex:5];
    //[indexSet addIndex:8];
    //[self.myTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    //[self.myTableView endUpdates];
    [self.myTableView reloadData];
	//NSLog(@"%@", documentsDirectory);
	return;
}


#pragma mark 
#pragma mark Helpers

#define CONST_Cell_height 44.0f
#define CONST_Cell_width 270.0f

#define CONST_textLabelFontSize     13
#define CONST_detailLabelFontSize   13

static UIFont *subFont;
static UIFont *titleFont;

- (UIFont*) TitleFont;
{
	if (!titleFont) titleFont = [UIFont boldSystemFontOfSize:CONST_textLabelFontSize];
	return titleFont;
}

- (UIFont*) SubFont;
{
	if (!subFont) subFont = [UIFont systemFontOfSize:CONST_detailLabelFontSize];
	return subFont;
}

- (UITableViewCell*) CreateMultilinesCell :(NSString*)cellIdentifier
{
	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle 
													reuseIdentifier:cellIdentifier];
	
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.font = [self TitleFont];
	
	cell.detailTextLabel.numberOfLines = 0;
	cell.detailTextLabel.font = [self SubFont];
	
	return cell;
}

- (int) heightOfCellWithTitle :(NSString*)titleText 
				   andSubtitle:(NSString*)subtitleText
{
	CGSize titleSize = {0, 0};
	CGSize subtitleSize = {0, 0};
	
	if (titleText && ![titleText isEqualToString:@""]) 
		titleSize = [titleText sizeWithFont:[self TitleFont] 
						  constrainedToSize:CGSizeMake(CONST_Cell_width, 4000) 
							  lineBreakMode:UILineBreakModeWordWrap];
	
	if (subtitleText && ![subtitleText isEqualToString:@""]) 
		subtitleSize = [subtitleText sizeWithFont:[self SubFont] 
								constrainedToSize:CGSizeMake(CONST_Cell_width, 4000) 
									lineBreakMode:UILineBreakModeWordWrap];
	
	return titleSize.height + subtitleSize.height;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *result = nil;
    static NSString *CellIdentifier = @"Cell";
    //result = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(indexPath.section == 0)
    {
        //NSLog(@"in");
        if (result == nil) {
            result = [self CreateMultilinesCell:CellIdentifier];
        }
        /*result.textLabel.text = @"1) Click on the DOWNLOAD link below. \n2) Grant permission to this app [first time only].\n3) From desktop upload your resume to Dropbox: \na. [ Dropbox\\Apps\\craigslist_jobs ] \nb.[ Dropbox folder\\ craigslist_jobs ] \n4)If there are no files, available, this might probably be the case, if its ur first time, then goto desktop and add files to the directory specified above.";*/
        result.textLabel.text = @"- Click on the DOWNLOAD link/button below. \n- Grant permission to this app (first time only).\n- Click on a file and download it to the phone. \n- Issues you might see: When you link this app to your dropbox for the first time, you might get an error.\n- You can resolve this by clicking the link/download button again on this page.";
        return result;
    }
    if(indexPath.section > 0 && indexPath.section < 5)
    {
        //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //NSString *str = cell.textLabel.text;
        //NSLog(@"out %@",str);
        if (result == nil){
            result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:CellIdentifier];
        } 
        
        result.textLabel.text = @"";
        
        //NSIndexPath *ipath = [NSIndexPath indexPathForRow:0 inSection:0];
        if(indexPath.section == 1 && indexPath.row == 0)
        {
            result.textLabel.text = @"CLICK HERE TO DOWNLOAD RESUMES";
            result.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            //result.textLabel.font = [UIFont fontWithName:@"ArialMT"];
            result.textLabel.font = [UIFont boldSystemFontOfSize:13];
        }
        else if(indexPath.section == 2)
        {
            result.textLabel.text = [directoryContent objectAtIndex:indexPath.row];
            
            // search the string which is added to the list
            // add the detailed text
            helperMethods *tmpinstance = [helperMethods alloc];
            NSArray *tmpCities = [tmpinstance fetchCities:@"DefaultResume"];
            NSString *tmparraycity;
            NSString *tmpCity = [self.directoryContent objectAtIndex:indexPath.row];
            result.detailTextLabel.text = @"";
            result.accessoryType = UITableViewCellAccessoryNone;
            [result setSelectionStyle:UITableViewCellSelectionStyleBlue];
            for( tmparraycity in tmpCities)
            {
                if ([tmparraycity isEqual:tmpCity]) {
                    // result.detailTextLabel.text = @"Added";
                    result.accessoryType = UITableViewCellAccessoryCheckmark;
                    [result setSelectionStyle:UITableViewCellSelectionStyleNone];
                }
            }
            result.textLabel.font = [UIFont boldSystemFontOfSize:13];
            // get the cities from the edit table
            result.textLabel.text = tmpCity;
            
        }
        else if(indexPath.section == 3)
        {
            result.textLabel.text = [directoryContent objectAtIndex:indexPath.row];
            
            // search the string which is added to the list
            // add the detailed text
            helperMethods *tmpinstance = [helperMethods alloc];
            NSArray *tmpCities = [tmpinstance fetchCities:@"DefaultCover"];
            NSString *tmparraycity;
            NSString *tmpCity = [self.directoryContent objectAtIndex:indexPath.row];
            result.detailTextLabel.text = @"";
            result.accessoryType = UITableViewCellAccessoryNone;
            [result setSelectionStyle:UITableViewCellSelectionStyleBlue];
            for( tmparraycity in tmpCities)
            {
                if ([tmparraycity isEqual:tmpCity]) {
                    // result.detailTextLabel.text = @"Added";
                    result.accessoryType = UITableViewCellAccessoryCheckmark;
                    [result setSelectionStyle:UITableViewCellSelectionStyleNone];
                }
            }
            result.textLabel.font = [UIFont boldSystemFontOfSize:13];
            // get the cities from the edit table
            result.textLabel.text = tmpCity;
            
        }
        else if(indexPath.section == 4)
        {
            result.textLabel.text = [directoryContent objectAtIndex:indexPath.row];
            
            // search the string which is added to the list
            // add the detailed text
            helperMethods *tmpinstance = [helperMethods alloc];
            NSArray *tmpCities = [tmpinstance fetchCities:@"DefaultEmailBody"];
            NSString *tmparraycity;
            NSString *tmpCity = [self.directoryContent objectAtIndex:indexPath.row];
            result.detailTextLabel.text = @"";
            result.accessoryType = UITableViewCellAccessoryNone;
            [result setSelectionStyle:UITableViewCellSelectionStyleBlue];
            for( tmparraycity in tmpCities)
            {
                if ([tmparraycity isEqual:tmpCity]) {
                    // result.detailTextLabel.text = @"Added";
                    result.accessoryType = UITableViewCellAccessoryCheckmark;
                    [result setSelectionStyle:UITableViewCellSelectionStyleNone];
                }
            }
            result.textLabel.font = [UIFont boldSystemFontOfSize:13];
            // get the cities from the edit table
            result.textLabel.text = tmpCity;
            
        }

    }
    
    return result;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if(indexPath.section == 1)
    {
        
         dropboxSelectResumes *detailViewController = [dropboxSelectResumes alloc];
         // Pass the selected object to the new view controller.
         [self.navigationController pushViewController:detailViewController animated:YES];
        
        //[filenameArray removeAllObjects];
        //[self didPressLink];
        //[[self restClient] loadMetadata:@"/"];
        //NSLog(@"--%d",[filenameArray count]);

    }
    if(indexPath.section == 2)
    {
        helperMethods *myInstance = [[helperMethods alloc] init];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [myInstance saveDefaultResume:cell.textLabel.text];
        [self.myTableView reloadData];
    }
    if(indexPath.section == 3)
    {
        helperMethods *myInstance = [[helperMethods alloc] init];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [myInstance saveDefaultCover:cell.textLabel.text];
        [self.myTableView reloadData];
    }
    if(indexPath.section == 4)
    {
        helperMethods *myInstance = [[helperMethods alloc] init];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString *tmpName = cell.textLabel.text;
        NSRange textRange;
        textRange =[[tmpName lowercaseString] rangeOfString:[@".txt" lowercaseString]];
        if(textRange.location == NSNotFound)
        {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Information"
                                      message:@"Only text files are allowed to be selected."
                                      delegate:nil
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil];
            [alertView show];
        }else
        {
            [myInstance saveDefaultEmailBody:cell.textLabel.text];
            [self.myTableView reloadData];
        }
    }
    
}

@end
