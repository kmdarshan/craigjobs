//
//  displayJobsPerCategoryCity.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface displayJobsPerCategoryCity : UITableViewController<UIAlertViewDelegate, MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    NSString *previousCity;
    NSString *jobCategory;
    NSMutableData *responseData;
    NSMutableArray *listOfPositions;
    NSMutableArray *listOfDates;
    NSMutableArray *listOfUrls;
    NSMutableArray *listCounties;
    NSMutableArray *listPostingIds;
    BOOL bShowDates;
    UIBarButtonItem *anotherButton;
    long long expectedLength;
	long long currentLength;
    NSString *desctablename;
    bool bFinishLoading;
    NSString *tmpErrorShown;
    NSString *previousCompany;
    
}
@property (nonatomic, retain) NSString *tmpErrorShown;
@property (nonatomic, retain) NSString *previousCity;
@property (nonatomic, retain) NSString *jobCategory;
@property (nonatomic, retain) NSMutableArray *listOfPositions;
@property (nonatomic, retain) NSMutableArray *listOfDates;
@property (nonatomic, retain) NSMutableArray *listOfUrls;
@property (nonatomic, retain) NSMutableArray *listCounties;
@property (nonatomic, retain) NSMutableArray *listPostingIds;
@property (nonatomic, retain) NSString *desctablename;
@property (nonatomic) BOOL bShowDates;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSString *previousCompany;
//-(NSString*) returnDatabaseTableDescName:(NSString *)tablename;
@end
