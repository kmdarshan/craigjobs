//
//  displayAllCitiesTableViewController.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface displayAllCitiesTableViewController : UITableViewController{

NSMutableArray *cities_;
}
@property (nonatomic, retain) NSMutableArray *cities;
- (void) reloadCityTables;
-(void) AddCities:(id)sender;
-(void) displayButtons;
@end
