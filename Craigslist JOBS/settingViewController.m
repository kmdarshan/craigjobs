//
//  settingViewController.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "settingViewController.h"
#import "helperMethods.h"
#import "displaySavedJobsTableController.h"
#import "displayAllCitiesTableViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
@implementation settingViewController
@synthesize myTableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section{
    if(section == 1 || section == 2 || section == 3)
        return 1;
    return 3;
}

- (NSString *) tableView:(UITableView *)tableView
 titleForHeaderInSection:(NSInteger)section{
    NSString *result = nil;
    if ([tableView isEqual:self.myTableView] &&
        section == 0){
        result = @"Jobs";
    }
    if ([tableView isEqual:self.myTableView] &&
        section == 1){
        result = @"Cities / Job types";
    }
    
    if ([tableView isEqual:self.myTableView] &&
        section == 2){
        result = @"Feedback";
    }
    if ([tableView isEqual:self.myTableView] &&
        section == 3){
        result = @"Delete";
    }

    return result;
}
- (NSString *) tableView:(UITableView *)tableView
 titleForFooterInSection:(NSInteger)section{
    NSString *result = nil;
    return result;
}

- (CGFloat) tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    CGFloat result = 0.0f;
    result = 30.0f;
    return result;
}
- (CGFloat) tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section{
    CGFloat result = 0.0f;
    return result;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
- (void)viewDidLoad{
    [super viewDidLoad];
    self.myTableView =
    [[UITableView alloc] initWithFrame:self.view.bounds
                                 style:UITableViewStyleGrouped];
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    self.myTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;

    [self.view addSubview:self.myTableView];
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *result = nil;
    static NSString *CellIdentifier = @"CellIdentifier";
    result = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (result == nil){
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:CellIdentifier];
    }
    if(indexPath.section == 0)
    {
        //if((long)indexPath.row == 0)
        //    result.textLabel.text = @"View saved Jobs";
        if((long)indexPath.row == 0)
            result.textLabel.text = @"Clear saved jobs";
        else if((long)indexPath.row == 1)
            result.textLabel.text = @"Clear recently viewed jobs";
        else if((long)indexPath.row == 2)
            result.textLabel.text = @"Clear recently searched jobs";
    }else if(indexPath.section == 1)
    {
        if((long)indexPath.row == 0)
            result.textLabel.text = @"Clear Cities";
        else if((long)indexPath.row == 1)
            result.textLabel.text = @"...";   
    }
    else if(indexPath.section == 2)
    {
        if((long)indexPath.row == 0)
            result.textLabel.text = @"Click here for Feedback";
        else if((long)indexPath.row == 1)
            result.textLabel.text = @"About";   
    }else
    {
        result.textLabel.text = @"Downloaded files on phone";
    }
    return result;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if((long)indexPath.row == 0)
        {
            // delete all jobs
            helperMethods *myInstance = [[helperMethods alloc] init];
            //[myInstance saveposition:tmpPosition :tmpdesc :email ];
            [myInstance deleteSavedJobs];
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"" message:@"Cleared saved Jobs"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        if((long)indexPath.row == 1)
        {
            // delete all jobs
            helperMethods *myInstance = [[helperMethods alloc] init];
            [myInstance emptyTable:@"Recentjobs"];
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"" message:@"Cleared recently viewed Jobs"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        if((long)indexPath.row == 2)
        {
            // delete all jobs
            helperMethods *myInstance = [[helperMethods alloc] init];
            [myInstance emptyTable:@"SavedSearches"];
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"" message:@"Cleared recently searched Jobs"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
    }
    
    if(indexPath.section == 1)
    {
        if((long)indexPath.row == 0)
        {
            // load the edit cities table
            helperMethods *tmpinstance = [helperMethods alloc];
            [tmpinstance emptyTable:@"EditCities"];
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"" message:@"Cleared all cities"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        }
        if((long)indexPath.row == 1)
        {
            // delete all jobs
            //helperMethods *myInstance = [[helperMethods alloc] init];
            //[myInstance saveposition:tmpPosition :tmpdesc :email ];
            //[myInstance deleteSavedJobs];
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"" message:@"...."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        
    }
    
    if(indexPath.section == 2)
    {
        if((long)indexPath.row == 0)
        {
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController *viewController = [[MFMailComposeViewController alloc] init];
                viewController.mailComposeDelegate = self;
                [viewController setSubject:@"Feedback"];
                [viewController setMessageBody:@"" isHTML:YES]; 
                NSArray *tmpRec = [NSArray alloc];
                tmpRec = [tmpRec initWithObjects:@"support@craigslistjobsin.us",nil];
                [viewController setToRecipients:tmpRec]; 
                [self presentModalViewController:viewController animated:YES];
                //[viewController release];
            }
        }
        if((long)indexPath.row == 1)
        {
            // delete all jobs
            //helperMethods *myInstance = [[helperMethods alloc] init];
            //[myInstance saveposition:tmpPosition :tmpdesc :email ];
            //[myInstance deleteSavedJobs];
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"" message:@"...."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        
    }
    if(indexPath.section == 3)
    {
        // For error information
        NSError *error;
        // Point to Document directory
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        //NSLog(documentsDirectory);
        NSFileManager *fM = [NSFileManager defaultManager];
        NSArray *fileList = [fM contentsOfDirectoryAtPath:documentsDirectory error:&error];
        
        // do not delete any sqlite stuff
        NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH 'pdf' OR self ENDSWITH '.txt' OR self ENDSWITH '.doc' OR self ENDSWITH '.docx'"];
        NSArray *onlyCvs = [fileList filteredArrayUsingPredicate:fltr];
        fileList = [NSArray arrayWithArray:onlyCvs];
        
        //NSMutableArray *directoryList = [[NSMutableArray alloc] init];
        for(NSString *file in fileList) {
            NSString *path = [documentsDirectory stringByAppendingPathComponent:file];
            if ([fM removeItemAtPath:path error:&error] != YES)
                NSLog(@"Unable to delete file: %@", [error localizedDescription]);

            //BOOL isDir = NO;
            //[fM fileExistsAtPath:path isDirectory:(&isDir)];
            //if(isDir) {
            //    [directoryList addObject:file];
            //}
        }
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Finished deleting the downloaded files."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        //NSLog(@"%@", directoryList);
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	[self dismissModalViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
