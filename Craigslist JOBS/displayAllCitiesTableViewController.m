//
//  displayAllCitiesTableViewController.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "displayAllCitiesTableViewController.h"
#import "displayJobCategoriesTableView.h"
#import "helperMethods.h"
#import "selectCities.h"
@implementation displayAllCitiesTableViewController
@synthesize cities=cities_;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) reloadCityTables
{
    helperMethods *myInstance = [[helperMethods alloc] init];
    NSArray *tmpArrayCities = [myInstance fetchCities:@"EditCities"];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddCities:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    cities_ = [NSMutableArray arrayWithCapacity:1];
    cities_ = [[NSMutableArray alloc] initWithArray:tmpArrayCities];
    [self.tableView reloadData];

}

-(void) displayButtons
{
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 90.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.translucent = self.navigationController.navigationBar.translucent;
    tools.barStyle = self.navigationController.navigationBar.barStyle;  
    tools.backgroundColor = self.navigationController.navigationBar.backgroundColor;
    tools.tintColor = self.navigationController.navigationBar.tintColor;
    //tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    // anyone know how to get it perfect?
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Create a standard refresh button.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc]
                           initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(commitEditingStyle:)];
    [buttons addObject:bi];
    
    // Create a spacer.
    bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    bi.width = 3.0f;
    [buttons addObject:bi];
    
    // Add profile button.
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddCities:)];
    editButton.style = UIBarButtonItemStyleBordered;
    [buttons addObject:editButton];
    
    // Add buttons to toolbar and toolbar to nav bar.
    [tools setItems:buttons animated:NO];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    self.navigationItem.rightBarButtonItem = twoButtons;   
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.tabBarController.moreNavigationController.navigationBar.tintColor = [UIColor purpleColor];
    // 0.5, 0.0, 0.5 RGB 
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:19/256.0 green:133/256.0 blue:45/256.0 alpha:1.0];
    self.tabBarController.moreNavigationController.navigationBar.tintColor = [UIColor colorWithRed:19/256.0 green:133/256.0 blue:45/256.0 alpha:1.0];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // load the database table
    helperMethods *myInstance = [[helperMethods alloc] init];
    NSArray *tmpArrayCities = [myInstance fetchCities:@"EditCities"];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddCities:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    cities_ = [NSMutableArray arrayWithCapacity:1];
    //NSBundle *mainBundle = [NSBundle mainBundle];
    //NSString *filePath   = [mainBundle pathForResource:@"cities" ofType:@"plist"];
    //cities_ = [[NSMutableArray alloc] initWithContentsOfFile:filePath];*/
    if ([tmpArrayCities count] == 0) {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Please add the cities, to view jobs in that city."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
    }
    cities_ = [[NSMutableArray alloc] initWithArray:tmpArrayCities];
}
-(void) AddCities:(id)sender
{
    selectCities *searchController = [selectCities alloc];
    UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
	temporaryBarButtonItem.title = @"Back";
	self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
    [self.navigationController pushViewController:searchController animated:YES];   
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setTitle:@"Cities"];
    [self reloadCityTables];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [cities_ count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *result = nil;
    
    static NSString *TableViewCellIdentifier = @"";
    result = [tableView
              dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (result == nil){
        result = [[UITableViewCell alloc]
                  initWithStyle:UITableViewCellStyleDefault
                  reuseIdentifier:TableViewCellIdentifier];
    }
    result.textLabel.text = [self.cities objectAtIndex:indexPath.row];
    result.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //result.textLabel.font = [UIFont fontWithName:@"ArialMT" size:17];
    result.textLabel.font = [UIFont boldSystemFontOfSize:17];
    return result;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        NSString *tmpcity = selectedCell.textLabel.text;
        // NSLog(@"deleteing style delete %@ ", tmpcity);
        helperMethods* myInstance = [helperMethods alloc];
        [myInstance deleteEditTable:tmpcity];
        
        // get the cities from this table
        NSArray* tmpArrayCities = [myInstance fetchCities:@"EditCities"];
        // NSLog(@"count => %d", [tmpArrayCities count]);
        cities_ = [[NSMutableArray alloc] initWithArray:tmpArrayCities];
        [self.tableView reloadData];
        // Delete the row from the data source
       //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSString *tmpString = [self.cities objectAtIndex:indexPath.row];
    displayJobCategoriesTableView *displayCitiesController = [displayJobCategoriesTableView alloc];
    displayCitiesController.selectedCity = tmpString;
    [self.navigationController pushViewController:displayCitiesController animated:YES];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */

    NSString *tmpString = [self.cities objectAtIndex:indexPath.row];
    UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
	temporaryBarButtonItem.title = tmpString;
	self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
    
    displayJobCategoriesTableView *displayCitiesController = [displayJobCategoriesTableView alloc];
    displayCitiesController.selectedCity = tmpString;
    [self.navigationController pushViewController:displayCitiesController animated:YES];
}

@end
