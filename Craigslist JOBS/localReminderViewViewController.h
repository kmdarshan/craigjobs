//
//  localReminderViewViewController.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 6/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface localReminderViewViewController : UIViewController
{
    UIDatePicker *datePicker;
    NSString *position;
}
@property (nonatomic, retain) NSString *position;
@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
- (void)cancelAddNotification;
- (void)addNotification;
@end
