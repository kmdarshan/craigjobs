//
//  selectCities.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "selectCities.h"
#import "helperMethods.h"
#import "displayAllCitiesTableViewController.h"
@implementation selectCities
@synthesize listOfAlphabets;
@synthesize dictCities;
@synthesize cities;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if([listOfAlphabets count]==0)
        return @"";
    return [listOfAlphabets objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	
	if(searching)
		return nil;
    
	return listOfAlphabets;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
	if(searching)
		return -1;
	
	NSInteger count = 0;
    for(NSString *character in listOfAlphabets)
    {
        if([character isEqualToString:title])
            return count;
        count ++;
    }
    return 0;// in case of some eror donot crash d application
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    cities = [NSMutableArray arrayWithCapacity:1];
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath   = [mainBundle pathForResource:@"cities" ofType:@"plist"];
    NSMutableArray *tmpCityArray = [NSMutableArray arrayWithCapacity:1];
    tmpCityArray = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    //cities = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    
    NSString *tmpCity;
    int tmpPos=0;
    for(tmpCity in tmpCityArray)
    {
        tmpCity = [tmpCity capitalizedString];
        [cities insertObject:tmpCity atIndex:tmpPos];
        tmpPos++;
    }
    
    dictCities = [[NSMutableDictionary alloc] init];
    listOfAlphabets = [[NSMutableArray alloc] init];
	[listOfAlphabets addObject:@"A"];
	[listOfAlphabets addObject:@"B"];
	[listOfAlphabets addObject:@"C"];
	[listOfAlphabets addObject:@"D"];
	[listOfAlphabets addObject:@"E"];
	[listOfAlphabets addObject:@"F"];
	[listOfAlphabets addObject:@"G"];
	[listOfAlphabets addObject:@"H"];
	[listOfAlphabets addObject:@"I"];
	[listOfAlphabets addObject:@"J"];
	[listOfAlphabets addObject:@"K"];
	[listOfAlphabets addObject:@"L"];
	[listOfAlphabets addObject:@"M"];
	[listOfAlphabets addObject:@"N"];
	[listOfAlphabets addObject:@"O"];
	[listOfAlphabets addObject:@"P"];
	[listOfAlphabets addObject:@"Q"];
	[listOfAlphabets addObject:@"R"];
	[listOfAlphabets addObject:@"S"];
	[listOfAlphabets addObject:@"T"];
	[listOfAlphabets addObject:@"U"];
	[listOfAlphabets addObject:@"V"];
	[listOfAlphabets addObject:@"W"];
	[listOfAlphabets addObject:@"X"];
	[listOfAlphabets addObject:@"Y"];
	[listOfAlphabets addObject:@"Z"];
    
    for (NSString *c in listOfAlphabets) {
        NSMutableArray *arrayOfNames = [[NSMutableArray alloc]init];
        for (NSString *tmpcomp in cities) {
            char tmpchar = [tmpcomp characterAtIndex:0];
            NSString *tmpstr = [NSString stringWithFormat:@"%c", tmpchar];
            tmpstr = [tmpstr capitalizedString];
            
            if([tmpstr isEqualToString:c])
            {
                
                //NSLog(@"adding %@",tmpcomp);
                [arrayOfNames addObject:tmpcomp];
                //[dictCompanies setObject:arrayOfNames forKey:[NSString stringWithFormat:@"%s",c]];
            }
        }
        //NSLog(@"=> %d", [arrayOfNames count]);
        //NSLog(c);
        [dictCities setObject:arrayOfNames forKey:c];
        //NSLog(@"XXX %d", [dictCompanies count]);
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [listOfAlphabets count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[dictCities objectForKey:[listOfAlphabets objectAtIndex:section]] count];
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"tmp_cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:MyIdentifier];
    }
    
    cell.textLabel.text = [[dictCompanies objectForKey:[listOfAlphabets objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *result = nil;
    
    static NSString *TableViewCellIdentifier = @"";
    result = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (result == nil){
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
    }
    // search the string which is added to the list
    // add the detailed text
    helperMethods *tmpinstance = [helperMethods alloc];
    NSArray *tmpCities = [tmpinstance fetchCities:@"EditCities"];
    NSString *tmparraycity;
    //NSString *tmpCity = [self.cities objectAtIndex:indexPath.row];
    NSString *tmpCity = [[dictCities objectForKey:[listOfAlphabets objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    result.detailTextLabel.text = @"";
    result.accessoryType = UITableViewCellAccessoryNone;
    [result setSelectionStyle:UITableViewCellSelectionStyleBlue];
    for( tmparraycity in tmpCities)
    {
        if ([tmparraycity isEqual:tmpCity]) {
            // result.detailTextLabel.text = @"Added";
            result.accessoryType = UITableViewCellAccessoryCheckmark;
            [result setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
    }
    // get the cities from the edit table
    //result.textLabel.text = [[dictCities objectForKey:[listOfAlphabets objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    result.textLabel.text = tmpCity;
    result.textLabel.font = [UIFont fontWithName:@"ArialMT" size:17];
    return result;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    NSString *tmpCity = [[dictCities objectForKey:[listOfAlphabets objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];

    helperMethods *tmpinstance = [helperMethods alloc];
    
    // get the cities from the database table
    NSArray *tmpCities = [tmpinstance fetchCities:@"EditCities"];
    int tmpint = [tmpCities indexOfObject:tmpCity];
    if (tmpint > 50000) { // this is some random values, for npos
        [tmpinstance addCity:tmpCity];   
    }
    [self.tableView reloadData];
    //UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"" message:@"City has been added."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    //[connectFailMessage show];
}

@end
