//
//  helperMethods.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "/usr/include/sqlite3.h"
#import <CoreData/CoreData.h>
@interface helperMethods : NSObject
{
    sqlite3 *sqldb;
    NSString        *databasePath;
    NSManagedObjectContext *managedObjectContext;
}
// this table concatentates the desc into a tablename
// which gives us the full table name description
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
-(NSString*) returnDatabaseTableDescName:(NSString *)tablename;
-(void) saveposition:(NSString *)position:(NSString *)desc:(NSString *)email;
-(void) saveData:(NSString *)position:(NSString *)desc:(NSString *)email;
-(void) saveDataCoreData:(NSString *)position:(NSString *)desc:(NSString *)email:(NSString *)city:(NSString *)compensation;
-(int) returnNumberOfJobs;
-(void) fillCitiesTable;
- (NSArray*) fetchCoreDataJobs:(NSString *)tablename;
- (void) saveDataCoreDataRecents:(NSString*) tablename:(NSString *)position:(NSString *)desc:(NSString *)email:(NSString *)city:(NSString *)compensation:(NSString*)url;
-(void) deleteSavedJobs;
- (int) getCountTableName:(NSString *) tablename;
- (NSArray*) fetchCities:(NSString*) tablename;
-(void) deleteEditTable:(NSString*) cityname;
- (void) fillInitEditTable;
-(void) emptyTable:(NSString*) tablename;
- (void) addCity:(NSString *)city;
- (void) saveDefaultResume:(NSString *)filename;
-(NSString*) getDefaultResume;
-(NSString*) getDefaultCover;
- (void) saveDataSearch:(NSString *)position:(NSString *)dept:(NSString *)city;
- (void) saveDefaultCover:(NSString *)filename;
- (void) addSearchSelectedValue:(NSString*)tablename:(NSString *)selectedvalue;
-(NSString*) getDefaultSelectedSearchValue:(NSString*)tablename;
- (void) saveDefaultEmailBody:(NSString *)filename;
-(NSString*) getDefaultEmailBody;
@end
