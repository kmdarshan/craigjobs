//
//  selectCities.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface selectCities : UITableViewController
{
    NSMutableArray *listOfAlphabets;
    BOOL searching;
    NSMutableArray *cities;
    NSMutableDictionary *dictCities;
}
@property (nonatomic, retain) NSMutableArray *cities;
@property (nonatomic, retain) NSMutableArray *listOfAlphabets;
@property (nonatomic, retain) NSMutableDictionary *dictCities;
@end
