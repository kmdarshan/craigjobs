//
//  displayJobCategoriesTableView.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface displayJobCategoriesTableView : UITableViewController{
    NSMutableArray *cities_;
    NSString *selectedCity;
    NSString *selectedCompany;
}
@property (nonatomic, retain) NSMutableArray *cities;
@property (nonatomic, retain) NSString *selectedCompany;
@property (nonatomic, retain) NSString *selectedCity;
@end
