//
//  searchResultsTableViewController.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "searchResultsTableViewController.h"
#import "helperMethods.h"
#import "JSON.h"
#import "displayJobDetails.h"
@implementation searchResultsTableViewController
@synthesize keywords;
@synthesize city;
@synthesize jobname;
@synthesize dept;
@synthesize responseData;
@synthesize tmpLoaded;
@synthesize listOfPositions = listOfPositions;
@synthesize listOfDates = listOfDates;
@synthesize listOfUrls = listOfUrls;
@synthesize listCounties = listCounties;
@synthesize listPostingIds = listPostingIds;
@synthesize bShowDates = bShowDates_;
@synthesize desctablename;
@synthesize tmpErrorShown;
-(id)initWithFrame:(CGRect)theFrame {
    if (self = [super init]) {
        frame = theFrame;
        self.view.frame = theFrame;
    }
    return self;
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)refresh:(id)sender
{
   // activityLabel.hidden = FALSE;
   // [activityIndicator startAnimating];
    tmpErrorShown = @"";
    helperMethods *myInstance = [[helperMethods alloc] init];
    NSString *tmpDeptTableName = [myInstance returnDatabaseTableDescName:self.dept];
    desctablename = tmpDeptTableName;
    NSString *tmpCity = self.city;
    NSString *tmpKeywords = self.keywords;
    // lets remove the last characters of the string, if its a whitespace
    NSString *tmpName = self.jobname;
    if ( [tmpName length] > 0)
        tmpName = [tmpName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];    
    
    // replace the city with the shortcut
    // san francisco -> sfbay
    NSString *tmpSelectedCity = [tmpCity lowercaseString];
    NSString *searchString = @"francisco";
    NSRange prefixRange = [tmpSelectedCity rangeOfString:searchString
                                                 options:(NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpSelectedCity = @"sfbay";
    }
    
    // remove all spaces
    // useful for searching the urls instead of city
    tmpSelectedCity = [tmpSelectedCity stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *urlAsString = @"";
    
    urlAsString = [urlAsString stringByAppendingString:@"city="];
    urlAsString = [urlAsString stringByAppendingString:tmpSelectedCity];
    urlAsString = [urlAsString stringByAppendingString:@"&keywords="];
    urlAsString = [urlAsString stringByAppendingString:tmpKeywords];
    urlAsString = [urlAsString stringByAppendingString:@"&jobname="];
    urlAsString = [urlAsString stringByAppendingString:tmpName];
    urlAsString = [urlAsString stringByAppendingString:@"&tablename="];
    urlAsString = [urlAsString stringByAppendingString:tmpDeptTableName];
    //NSLog(@"%@", urlAsString); 
    
    /*NSURLRequest *theRequest = 
     [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
     cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
     timeoutInterval:10.0];*/
    NSURL *tmp_url = [NSURL URLWithString:@"http://69.194.193.163/cgj/searchJobs.php"];    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:tmp_url];
    [urlRequest setTimeoutInterval:50.0f];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[urlAsString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    if (theConnection) {
        
        self.responseData = [NSMutableData data];
        
    } else {
        
        if([tmpErrorShown length] < 1)
        {
            tmpErrorShown = @"error";
            //activityLabel.hidden = TRUE;
            //[activityIndicator stopAnimating];
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Internet connection Error" message:@"Error connecting to the internet."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
    }
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    


}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Create a standard refresh button.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc]
                           initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
    self.navigationItem.rightBarButtonItem = bi;
    
    
    helperMethods *myInstance = [[helperMethods alloc] init];
    NSString *tmpDeptTableName = [myInstance returnDatabaseTableDescName:self.dept];
    desctablename = tmpDeptTableName;
    NSString *tmpCity = self.city;
    NSString *tmpKeywords = self.keywords;
    NSString *tmpName = self.jobname;
    if ( [tmpName length] > 0)
        tmpName = [tmpName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // replace the city with the shortcut
    // san francisco -> sfbay
    NSString *tmpSelectedCity = [tmpCity lowercaseString];
    NSString *searchString = @"francisco";
    NSRange prefixRange = [tmpSelectedCity rangeOfString:searchString
                                                 options:(NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpSelectedCity = @"sfbay";
    }
    
    // remove all spaces
    // useful for searching the urls instead of city
    tmpSelectedCity = [tmpSelectedCity stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *urlAsString = @"";
    
    urlAsString = [urlAsString stringByAppendingString:@"city="];
    urlAsString = [urlAsString stringByAppendingString:tmpSelectedCity];
    urlAsString = [urlAsString stringByAppendingString:@"&keywords="];
    urlAsString = [urlAsString stringByAppendingString:tmpKeywords];
    urlAsString = [urlAsString stringByAppendingString:@"&jobname="];
    urlAsString = [urlAsString stringByAppendingString:tmpName];
    urlAsString = [urlAsString stringByAppendingString:@"&tablename="];
    urlAsString = [urlAsString stringByAppendingString:tmpDeptTableName];
    //NSLog(@"%@", urlAsString); 
    
    /*NSURLRequest *theRequest = 
    [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                     cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                 timeoutInterval:10.0];*/
    
    NSURL *tmp_url = [NSURL URLWithString:@"http://69.194.193.163/cgj/searchJobs.php"];    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:tmp_url];
    [urlRequest setTimeoutInterval:50.0f];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[urlAsString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    if (theConnection) {
       
        //[activityIndicator startAnimating];
       self.responseData = [NSMutableData data];
        
    } else {
        
        if([tmpErrorShown length] < 1)
        {
            tmpErrorShown = @"error";
            //activityLabel.hidden = TRUE;
            //[activityIndicator stopAnimating];
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Internet connection Error" message:@"Error connecting to the internet."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
    }
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
    HUD.mode = MBProgressHUDModeDeterminate;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    //NSLog(@"receive data");
    if ([data length] > 0){
        //NSLog(@"%lu bytes of data was returned.", (unsigned long)[data length]);
        
    }else
    {
        if([tmpErrorShown length] < 1)
        {
            tmpErrorShown = @"error";

        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Error receiving data from server due to internet connectivity. Please check back again."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
        }
        
        //activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];
    }
    
	[responseData appendData:data];
    currentLength += [data length];
	HUD.progress = currentLength / (float)expectedLength;

    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //[HUD hide:YES];
	NSString *errorMessage = [NSString stringWithFormat:@"Connection failed: %@", [error description]];
    NSLog(@"connection failed %@", errorMessage);
    
    if([tmpErrorShown length] < 1)
    {
        tmpErrorShown = @"error";

    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"Sorry.No Internet Connection/Request timed out. Please try again."
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
    [alertView show];
       // activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];

    }
    [HUD hide:YES afterDelay:1];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    //NSLog(@"response val");
    //NSLog(responseData);
    NSString *responseString = [[NSString alloc] initWithData:responseData
                                                     encoding:NSUTF8StringEncoding];
    //NSLog(responseString);
    NSArray *userData = [responseString JSONValue];
    int elements = [userData count];
    if (elements < 1) {
        if([tmpErrorShown length] < 1)
        {
            tmpErrorShown = @"error";

        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"No jobs opening available at this moment. Please check back again or try refreshing/reloading the page."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
        }
        
        //activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];
    }else
    {
        NSMutableArray *responsePositions = [NSMutableArray array];
        NSMutableArray *responseDate = [NSMutableArray array];
        NSMutableArray *responseUrls = [NSMutableArray array];
        NSMutableArray *responseCounties = [NSMutableArray array];
        NSMutableArray *responsePostingIds = [NSMutableArray array];
        
        for (NSDictionary *user in userData) {
            [responsePositions addObject:[user objectForKey:@"position"]];
            [responseDate addObject:[user objectForKey:@"date"]];
            [responseUrls addObject:[user objectForKey:@"url"]];
            [responseCounties addObject:[user objectForKey:@"city"]];
            [responsePostingIds addObject:[user objectForKey:@"posting_id"]];
            
        }
        self.listOfPositions = responsePositions;
        self.listOfDates = responseDate;
        self.listOfUrls = responseUrls;
        self.listCounties = responseCounties;
        self.listPostingIds = responsePostingIds;
        
        [self.tableView reloadData];
       // activityLabel.hidden = TRUE;
       // [activityIndicator stopAnimating];
    }
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    HUD.mode = MBProgressHUDModeCustomView;
	[HUD hide:YES afterDelay:1];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.tmpLoaded == @"false")
    {
        self.tmpLoaded = @"true";
        //[activityIndicator startAnimating];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   // [activityIndicator stopAnimating];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return [self.listOfPositions count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSLog(@"hi");
    UITableViewCell *result = nil;
    static NSString *TableViewCellIdentifier = @"MyCells";
    result = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (result == nil){
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableViewCellIdentifier];
    }
    
    result.textLabel.text = [self.listOfPositions objectAtIndex:indexPath.row];
    result.detailTextLabel.text = [self.listOfDates objectAtIndex:indexPath.row];
    
    result.detailTextLabel.font = [UIFont fontWithName:@"ArialMT" size:11];
    result.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    result.textLabel.font = [UIFont fontWithName:@"ArialMT" size:14];
    return result;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tmpPosition = [self.listOfPositions objectAtIndex:indexPath.row];
    NSString *tmpNumberPostingIds = [self.listPostingIds objectAtIndex:indexPath.row];
    displayJobDetails *displayCitiesController = [displayJobDetails alloc];
    displayCitiesController.position = tmpPosition; 
    displayCitiesController.postingid = tmpNumberPostingIds;
    displayCitiesController.description = @"";
    displayCitiesController.load = @"yes";
    //NSString *tmpVal = [self.listOfUrls objectAtIndex:indexPath.row];
    displayCitiesController.url = [self.listOfUrls objectAtIndex:indexPath.row];
    displayCitiesController.city = [self.listCounties objectAtIndex:indexPath.row];
    displayCitiesController.tablename = desctablename;
    [self.navigationController pushViewController:displayCitiesController animated:YES];
}

@end
