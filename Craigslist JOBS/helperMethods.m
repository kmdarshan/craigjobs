//
//  helperMethods.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "helperMethods.h"
#import "redflowerAppDelegate.h"
@implementation helperMethods
@synthesize managedObjectContext;
-(NSString*)returnDatabaseTableDescName:(NSString *)tmpJobCategory
{
    // set the table name based on search categories
    NSString *searchString = @"food";
    NSString *beginsTest = tmpJobCategory;
    NSRange prefixRange = [beginsTest rangeOfString:searchString
                                            options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    NSString *tmpTableName=@"software";
    if(prefixRange.length > 0)
    {
        tmpTableName = @"food_bev_hosp";
    }
    searchString = @"accounting";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"accounting";
    }
    searchString = @"admin";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"admin_office";
    }
    
    searchString = @"engineering";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSLiteralSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"arch_engineering";
    }
    searchString = @"media";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSLiteralSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"art_media_design";
    }
    searchString = @"biotech";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"biotech_science";
    }
    searchString = @"business";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"business_mgmt";
    }
    searchString = @"customer";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"customer_service";
    }
    searchString = @"education";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"education";
    }
    searchString = @"[etc";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"etc";
    }
    searchString = @"general";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"general_labor";
    }
    
    searchString = @"government";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"government";
    }
    
    searchString = @"human";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"human_resources";
    }
    
    searchString = @"internet";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"internet_engineers";
    }
    
    searchString = @"legal";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"legal_paralegal";
    }
    
    searchString = @"manufacturing";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"manufacturing";
    }
    
    searchString = @"marketing";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"marketing_pr_ad";
    }
    
    searchString = @"medical";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"medical_health";
    }
    
    searchString = @"profit";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSLiteralSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"nonprofit_sector";
    }
    
    searchString = @"part";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"part_time";
    }
    
    searchString = @"real";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"real_estate";
    }
    
    searchString = @"retail";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"retail_wholesale";
    }
    
    searchString = @"sales";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"sales_bizdev";
    }
    
    searchString = @"salon";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"salon_spa_fitness";
    }
    
    searchString = @"security";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"security";
    }
    
    searchString = @"trade";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSLiteralSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"skilledtrade_craft";
    }
    
    searchString = @"software";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"software";
    }
    
    searchString = @"systems";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"systems_network";
    }
    
    searchString = @"technical";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"tech_support";
    }
    
    searchString = @"transport";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"transport";
    }
    
    searchString = @"film";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSLiteralSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"tv_film_video";
    }
    
    searchString = @"web";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"web_infodesign";
    }
    
    searchString = @"writing";
    prefixRange = [beginsTest rangeOfString:searchString
                                    options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
    if(prefixRange.length > 0)
    {
        tmpTableName = @"writing_editing";
    }
    
    return tmpTableName;
}

- (void) saveData:(NSString *)position:(NSString *)desc:(NSString *)email
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &sqldb) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO SAVEDJOBS (position, desc, email) VALUES (\"%@\", \"%@\", \"%@\")", position, desc, email];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(sqldb, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"data inserted");
        } else {
            NSLog(@"failed to add data");
        }
        sqlite3_finalize(statement);
        sqlite3_close(sqldb);
    }
}

-(void) saveposition:(NSString *)position:(NSString *)desc:(NSString *)email
{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"jobs.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    // NSLog(databasePath);
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
		const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &sqldb) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS SAVEDJOBS (ID INTEGER PRIMARY KEY AUTOINCREMENT, position TEXT, desc TEXT, email TEXT)";
            
            if (sqlite3_exec(sqldb, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table");
                return;
            }
            
            sqlite3_close(sqldb);
            
        } else {
            NSLog(@"Failed to open/create database");
            return;
        }
    }
    [self saveData:position :desc :email];
}
-(int) returnNumberOfJobs
{
    sqlite3_stmt    *statement;
    int jcount=0;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &sqldb) == SQLITE_OK)
    {
        const char *queryStmt = "SELECT * FROM SAVEDJOBS";
        int retVal = sqlite3_prepare_v2(sqldb, queryStmt, -1, &statement, NULL);
        NSLog(@"%d",retVal);
        if(retVal==SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                //jcount = sqlite3_column_int(statement, 0);
                NSLog(@"data inserted");
            } else {
                NSLog(@"failed to retrieve data");
            }    
        }
        sqlite3_finalize(statement);
        sqlite3_close(sqldb);
    }

    return 0;
}

-(void) deleteEditTable:(NSString *)cityname
{
    
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"EditCities" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * car in fetchedObjects) {
        
        NSString *tmpstr = [car valueForKey:@"name"];
        if ([tmpstr isEqualToString:cityname] == true)
        {
            // NSLog(@"deleted %@", tmpstr);
            [context deleteObject:car];
        }
    }
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
}

- (void) fillInitEditTable
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath   = [mainBundle pathForResource:@"cities" ofType:@"plist"];
    NSArray *tmparray = [NSArray arrayWithContentsOfFile:filePath];
    
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    for (int i=0; i < [tmparray count]; i++) {
        NSManagedObject *addCity = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"EditCities" 
                                    inManagedObjectContext:context];
        
        [addCity setValue:[tmparray objectAtIndex:i] forKey:@"name"];
    }
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}

- (void) fillCitiesTable
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath   = [mainBundle pathForResource:@"cities" ofType:@"plist"];
    NSArray *tmparray = [NSArray arrayWithContentsOfFile:filePath];
    
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    for (int i=0; i < [tmparray count]; i++) {
        NSManagedObject *addCity = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Cities" 
                                    inManagedObjectContext:context];

        [addCity setValue:[tmparray objectAtIndex:i] forKey:@"name"];
    }
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}

- (NSArray*) fetchCities:(NSString*) tablename
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:tablename inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    int count = [fetchedObjects count];
    NSMutableArray *returnArray = [[NSMutableArray alloc] initWithCapacity:count];
    for (NSManagedObject *info in fetchedObjects) {
        NSString *tmpstr = [info valueForKey:@"name"];
        [returnArray addObject:tmpstr];
    }
    NSArray *myArray = [returnArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return myArray;
}

- (int) getCountTableName:(NSString *) tablename
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:tablename inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    return [fetchedObjects count];
}

- (void) saveDataCoreDataRecents:(NSString*) tablename:(NSString *)position:(NSString *)desc:(NSString *)email:(NSString *)city:(NSString *)compensation:(NSString*)url
{
    
    // check for saved jobs and find the similar jobs
    // we dont want to insert duplicate entries
    NSArray *savedjobs = [self fetchCoreDataJobs:@"Recentjobs"];
    NSManagedObject *info;
    for (info in savedjobs) {
        NSString *tmpname = [info valueForKey:@"jobname"];
        if ([tmpname isEqualToString:position]) {
            //NSLog(@"already present");
            return;
        }
        //NSLog(@"--- Name: %@", [info valueForKey:@"jobname"]);
    }
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSManagedObject *failedBankInfo = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"Recentjobs" 
                                       inManagedObjectContext:context];
    
    [failedBankInfo setValue:email forKey:@"email"];
    [failedBankInfo setValue:desc forKey:@"desc"];
    [failedBankInfo setValue:position forKey:@"jobname"];
    [failedBankInfo setValue:city forKey:@"city"];
    [failedBankInfo setValue:compensation forKey:@"compensation"];
     [failedBankInfo setValue:url forKey:@"url"];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }

    //NSLog(@"saved desc....");
    
}

- (void) saveDataCoreData:(NSString *)position:(NSString *)desc:(NSString *)email:(NSString *)city:(NSString *)compensation
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    
    NSArray *savedjobs = [self fetchCoreDataJobs:@"Savedjobs"];
    NSManagedObject *info;
    for (info in savedjobs) {
        NSString *tmpname = [info valueForKey:@"jobname"];
        if ([tmpname isEqualToString:position]) {
            //NSLog(@"already present");
            return;
        }
        //NSLog(@"--- Name: %@", [info valueForKey:@"jobname"]);
    }
    
    NSManagedObject *failedBankInfo = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"Savedjobs" 
                                       inManagedObjectContext:context];
    
    [failedBankInfo setValue:email forKey:@"email"];
    [failedBankInfo setValue:desc forKey:@"desc"];
    [failedBankInfo setValue:position forKey:@"jobname"];
    [failedBankInfo setValue:city forKey:@"city"];
    [failedBankInfo setValue:compensation forKey:@"compensation"];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}

-(void) deleteSavedJobs
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"Savedjobs" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject * car in fetchedObjects) {
        [context deleteObject:car];
    }
}

- (void) saveDefaultResume:(NSString *)filename{
    
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"DefaultResume" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject * car in fetchedObjects) {
        [context deleteObject:car];
    }

    NSManagedObject *failedBankInfo = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"DefaultResume" 
                                       inManagedObjectContext:context];
    
    [failedBankInfo setValue:filename forKey:@"name"];
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}


- (void) saveDefaultEmailBody:(NSString *)filename{
    
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"DefaultEmailBody" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject * car in fetchedObjects) {
        [context deleteObject:car];
    }
    
    NSManagedObject *failedBankInfo = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"DefaultEmailBody" 
                                       inManagedObjectContext:context];
    
    [failedBankInfo setValue:filename forKey:@"name"];
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}


- (void) saveDefaultCover:(NSString *)filename{
    
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"DefaultCover" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject * car in fetchedObjects) {
        [context deleteObject:car];
    }
    
    NSManagedObject *failedBankInfo = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"DefaultCover" 
                                       inManagedObjectContext:context];
    
    [failedBankInfo setValue:filename forKey:@"name"];
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}
- (NSArray*) fetchCoreDataJobs:(NSString *)tablename
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:tablename inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    /*
     for (NSManagedObject *info in fetchedObjects) {
     NSLog(@"--- Name: %@", [info valueForKey:@"jobname"]);
     NSLog(@"--- desc: %@", [info valueForKey:@"desc"]);
     NSLog(@"--- email: %@", [info valueForKey:@"email"]);
     }*/
    return fetchedObjects;
}

-(void) emptyTable:(NSString*) tablename
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:tablename inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject * car in fetchedObjects) {
        [context deleteObject:car];
    }
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save the city: %@", [error localizedDescription]);
    }
}

- (void) addCity:(NSString *)city
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    
    NSManagedObject *failedBankInfo = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"EditCities" 
                                       inManagedObjectContext:context];
    [failedBankInfo setValue:city forKey:@"name"];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save the city: %@", [error localizedDescription]);
    }
}

- (void) addSearchSelectedValue:(NSString*)tablename:(NSString *)selectedvalue
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    
    NSManagedObject *failedBankInfo = [NSEntityDescription
                                       insertNewObjectForEntityForName:tablename
                                       inManagedObjectContext:context];
    [failedBankInfo setValue:selectedvalue forKey:@"name"];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save the city: %@", [error localizedDescription]);
    }
}

-(NSString*) getDefaultSelectedSearchValue:(NSString*)tablename
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:tablename inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSString *tmpName;
    for (NSManagedObject *info in fetchedObjects) {
        //NSLog(@"--- Name: %@", [info valueForKey:@"name"]);
        tmpName = [info valueForKey:@"name"];
    }
    return tmpName;
}


-(NSString*) getDefaultResume
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"DefaultResume" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSString *tmpName;
     for (NSManagedObject *info in fetchedObjects) {
         //NSLog(@"--- Name: %@", [info valueForKey:@"name"]);
         tmpName = [info valueForKey:@"name"];
     }
    return tmpName;
}

-(NSString*) getDefaultEmailBody
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"DefaultEmailBody" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSString *tmpName;
    for (NSManagedObject *info in fetchedObjects) {
        //NSLog(@"--- Name: %@", [info valueForKey:@"name"]);
        tmpName = [info valueForKey:@"name"];
    }
    return tmpName;
}

-(NSString*) getDefaultCover
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"DefaultCover" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSString *tmpName;
    for (NSManagedObject *info in fetchedObjects) {
        //NSLog(@"--- Name: %@", [info valueForKey:@"name"]);
        tmpName = [info valueForKey:@"name"];
    }
    return tmpName;
}
- (void) saveDataSearch:(NSString *)position:(NSString *)dept:(NSString *)city
{
    NSManagedObjectContext *context = [(redflowerAppDelegate *)[[UIApplication sharedApplication] delegate]  managedObjectContext];
    
    NSManagedObject *failedBankInfo = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"SavedSearches" 
                                       inManagedObjectContext:context];
    
    [failedBankInfo setValue:position forKey:@"name"];
    [failedBankInfo setValue:city forKey:@"city"];
    [failedBankInfo setValue:dept forKey:@"dept"];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
}
@end
