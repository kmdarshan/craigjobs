//
//  searchJobsViewController.h
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface searchJobsViewController : UIViewController<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    NSArray            *cityNames;
    NSArray            *deptNames;
    UIPickerView* picker;
    UITextField *jobnameTextFieldReturn;
    UITextField *keywordReturn;
    UITableView *cityTableView;
    UITableView *jobtypeTableView;
    UILabel *cityLabel;
    UILabel *jobtypeLabel;
    UITextField *cityTextfield;
    UITextField *deptTextfield;
    UISegmentedControl *segmentedControl;
    UIButton *addButtonCity;
    UIButton *addButtonDept;
    NSString *setterDeptField;
    NSString *setterCityField;
}
@property (retain, nonatomic) NSString *setterDeptField;
@property (retain, nonatomic) NSString *setterCityField;
- (IBAction)searchjobsbutton:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *addButtonCity;
@property (retain, nonatomic) IBOutlet UIButton *addButtonDept;
@property (weak, nonatomic) IBOutlet UITableView *tableSearchJobs;
//@property (nonatomic,retain) IBOutlet UISegmentedControl *segmentedControl;
- (IBAction)segmentControlClick:(id)sender;
- (IBAction)jobtypeButton:(id)sender;
@property (retain,nonatomic) IBOutlet UILabel *jobtypeLabel;
@property (retain, nonatomic) IBOutlet UILabel *cityLabel;
@property (nonatomic, retain) NSMutableArray *cities;
- (IBAction)showCityTable:(id)sender;
- (IBAction)selectCity:(id)sender;
- (IBAction)backgroundTouchedKeywords:(id)sender;
- (IBAction)backgroundTouched:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *jobnameTextFieldReturn;
- (IBAction)touchInsideKeywords:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *keywordTextFieldReturn;
- (IBAction)searchJobsAction:(id)sender;
@property (nonatomic, retain) NSArray *deptNames;
@property (weak, nonatomic) IBOutlet UIPickerView *deptPicker;
@property (nonatomic, retain) NSArray *cityNames;
- (IBAction)editingChangedPositions:(id)sender;
@property (weak, nonatomic) IBOutlet UIPickerView *cityPickerOutlet;
@property (nonatomic, strong) UIPickerView *picker;
-(IBAction)jobNameActiontextFieldReturn:(id)sender;
@property (nonatomic, retain) UITableView *cityTableView;
@property (nonatomic, retain) UITableView *jobtypeTableView;
@property (strong, nonatomic) IBOutlet UITextField *cityTextfield;
@property (strong, nonatomic) IBOutlet UITextField *deptTextfield;
-(IBAction)notifyButtonClicked:(id)sender;
- (void) insertUdid:(NSString *)udid:(NSString *)table:(NSString*)city:(NSString *)keywords;
-(void) successNotification;
@end
