//
//  displayJobDetails.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "displayJobDetails.h"
#import "displaySendOptions.h"
#import "JSON.h"
#import "sendEmail.h"
#import "helperMethods.h"
#import "Appirater.h"
#import "localReminderViewViewController.h"
@implementation displayJobDetails
@synthesize posLabel;
@synthesize position;
@synthesize description;
@synthesize descTextView=descTextView_;
@synthesize city=city_;
@synthesize url=url_;
@synthesize postingid=postingid_;
@synthesize tablename=tablename_;
@synthesize listOfItems;
@synthesize lenDesc=lenDesc_;
@synthesize responseData;
@synthesize progressView;
@synthesize load;
@synthesize actionSheet=_actionSheet;
@synthesize forwardButton;
@synthesize g_city=g_city_;
@synthesize g_position=g_position_;
@synthesize g_email=g_email_;
@synthesize g_url=g_url_;
@synthesize fromWhere;
@synthesize savedJobsPosition;
@synthesize g_reloadUrlString;
@synthesize twitterController;
-(id)initWithFrame:(CGRect)theFrame {
    if (self = [super init]) {
        //frame = theFrame;
        self.view.frame = theFrame;
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.fromWhere != @"saved") {
        //[activityIndicator startAnimating];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //[activityIndicator stopAnimating];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void) refresh:(id)sender
{
    if (self.tabBarController.selectedIndex == 3 || self.tabBarController.selectedIndex == 5)
    {
        // do nothing this data is from a database thats,why
    }else
    if(self.tabBarController.selectedIndex == 0)
    {
        
        UIApplication* app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = YES;
        //activityLabel.hidden = FALSE;
        //[activityIndicator startAnimating];
        self.descTextView.text = self.description;
        self.posLabel.text = self.position;
        
        NSString *tmpTablename = self.tablename;
        NSString *tmpPostingid = self.postingid;
        g_url = self.url;
        
        //NSString *tmpParams = [@"?posting_id=" stringByAppendingFormat:tmpMutablePostingId];
        NSString *tmpURLCity = [@"&tablename=" stringByAppendingString:tmpTablename];
        NSString *tmpParams = [@"?posting_id=" stringByAppendingString:tmpPostingid];
        tmpParams = [tmpParams stringByAppendingString:tmpURLCity];
        //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/getDescViaPostingId.php";
        NSString *urlAsString = @"http://69.194.193.163/cgj/getDescViaPostingId.php";
        urlAsString = [urlAsString stringByAppendingString:tmpParams];
        
        g_reloadUrlString = urlAsString;
        // Create the request.
        NSURLRequest *theRequest = 
        [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                         cachePolicy:NSURLRequestReloadRevalidatingCacheData
                     timeoutInterval:30.0];
        
        // create the connection with the request
        // and start loading the data
        // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            
            self.responseData = [NSMutableData data];
            listOfItems = [[NSMutableArray alloc] init];
            
            NSArray *arrPosition = [NSArray arrayWithObject:self.position];
            NSDictionary *dictPosition = [NSDictionary dictionaryWithObject:arrPosition forKey:@"data"];
            
            NSArray *arrCity = [NSArray arrayWithObject:self.city];
            NSDictionary *dictCity = [NSDictionary dictionaryWithObject:arrCity forKey:@"data"];
            
            NSArray *arrDescription = [NSArray arrayWithObject:self.description];
            NSDictionary *dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
            
            [listOfItems addObject:dictPosition];
            [listOfItems addObject:dictCity];
            [listOfItems addObject:dictDesc]; // this is for email. add it later
            [listOfItems addObject:dictDesc]; // compensation
            [listOfItems addObject:dictDesc];
            
            
        } else {
            
            // Inform the user that the connection failed.
            UIApplication* app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = NO;
            //activityLabel.hidden = TRUE;
            //[activityIndicator stopAnimating];
            
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Internet connection error" message:@"Try refreshing the page again."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        }   

    }else{
        
        UIApplication* app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = YES;
        
        self.descTextView.text = self.description;
        self.posLabel.text = self.position;
        NSString *tmpPostingid = self.postingid;
        g_url = self.url;
        //NSLog(tmpPostingid);
        //NSString *tmpParams = [@"?posting_id=" stringByAppendingFormat:tmpMutablePostingId];
        NSString *tmpURLCity = [@"&mostvieweddesc=" stringByAppendingString:@"yes"];
        NSString *tmpParams = [@"?posting_id=" stringByAppendingString:tmpPostingid];
        tmpParams = [tmpParams stringByAppendingString:tmpURLCity];
        //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/getMostViewed.php";
        NSString *urlAsString = @"http://69.194.193.163/cgj/getMostViewed.php";
        urlAsString = [urlAsString stringByAppendingString:tmpParams];
        g_reloadUrlString = urlAsString;
        //NSLog(urlAsString);
        // Create the request.
        NSURLRequest *theRequest = 
        [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                         cachePolicy:NSURLRequestReloadRevalidatingCacheData
                     timeoutInterval:30.0];
        
        // create the connection with the request
        // and start loading the data
        // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            
            self.responseData = [NSMutableData data];
            listOfItems = [[NSMutableArray alloc] init];
            
            NSArray *arrPosition = [NSArray arrayWithObject:self.position];
            NSDictionary *dictPosition = [NSDictionary dictionaryWithObject:arrPosition forKey:@"data"];
            
            NSArray *arrCity = [NSArray arrayWithObject:self.city];
            NSDictionary *dictCity = [NSDictionary dictionaryWithObject:arrCity forKey:@"data"];
            
            NSArray *arrDescription = [NSArray arrayWithObject:self.description];
            NSDictionary *dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
            
            [listOfItems addObject:dictPosition];
            [listOfItems addObject:dictCity];
            [listOfItems addObject:dictDesc]; // this is for email. add it later
            [listOfItems addObject:dictDesc];
            [listOfItems addObject:dictDesc];
            
            
        } else {
            
            // Inform the user that the connection failed.
            UIApplication* app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = NO;
            
            //activityLabel.hidden = TRUE;
            //[activityIndicator stopAnimating];
            
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        } 
    }
}

- (void) reminder:(id)sender
{ 
    localReminderViewViewController *setNotificationViewController = [[localReminderViewViewController alloc] initWithNibName:@"localReminderViewViewController" bundle:nil];
    //NSLog(@"Inside reminder %@", self.position);
    setNotificationViewController.position = self.position;
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:setNotificationViewController];
	[self presentModalViewController:navController animated:YES];
}

- (void) displayButtons
{
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.translucent = self.navigationController.navigationBar.translucent;
    tools.barStyle = self.navigationController.navigationBar.barStyle;  
    tools.backgroundColor = self.navigationController.navigationBar.backgroundColor;
    tools.tintColor = self.navigationController.navigationBar.tintColor;
    //tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    // anyone know how to get it perfect?
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:4];
    
    // Create a standard refresh button.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc]
                           initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
    [buttons addObject:bi];
    
    // Create a spacer.
    bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    bi.width = 3.0f;
    [buttons addObject:bi];
    
    UIBarButtonItem *reminder = [[UIBarButtonItem alloc] initWithTitle:@"Set Reminder" style:UIBarButtonItemStylePlain target:self action:@selector(reminder:)];
    reminder.style = UIBarButtonItemStyleBordered;
    [buttons addObject:reminder];
    
    // Create a spacer.
    bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    bi.width = 3.0f;
    [buttons addObject:bi];
    
    // Add profile button.
    // Do any additional setup after loading the view from its nib.
    forwardButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(clickDisplaySendOptions:)];
    forwardButton.style = UIBarButtonItemStyleBordered;
    [buttons addObject:forwardButton];
    
    // Add buttons to toolbar and toolbar to nav bar.
    [tools setItems:buttons animated:NO];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    self.navigationItem.rightBarButtonItem = twoButtons;   
}


- (void) insertMostViewedJobs:(NSString *)t_postingid:(NSString *)t_position:(NSString*)t_url:(NSString *)desc:(NSString *)email:(NSString *)t_city:(NSString *)compensation
{
    NSString *tmpParams = [@"posting_id=" stringByAppendingString:t_postingid];
    NSString *tmpposition = [@"&position=" stringByAppendingString:t_position];
    tmpParams = [tmpParams stringByAppendingString:tmpposition];
    NSString *tmpVar = [@"&url=" stringByAppendingString:t_url];
    tmpParams = [tmpParams stringByAppendingString:tmpVar];
    tmpVar = [@"&desc=" stringByAppendingString:desc];
    tmpParams = [tmpParams stringByAppendingString:tmpVar];
    tmpVar = [@"&email=" stringByAppendingString:email];
    tmpParams = [tmpParams stringByAppendingString:tmpVar];
    tmpVar = [@"&city=" stringByAppendingString:t_city];
    tmpParams = [tmpParams stringByAppendingString:tmpVar];
    tmpVar = [@"&compensation=" stringByAppendingString:compensation];
    tmpParams = [tmpParams stringByAppendingString:tmpVar];
    
                 
    //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/mostviewedUpdateTable.php";
    NSString *urlAsString = @"http://69.194.193.163/cgj/mostviewedUpdateTable.php";
    //urlAsString = [urlAsString stringByAppendingString:tmpParams];
    //NSLog(@"%@",tmpParams);
    
    NSURL *tmp_url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:tmp_url];
    [urlRequest setTimeoutInterval:50.0f];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[tmpParams dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLResponse *response = nil;
    NSError *error = nil;
    //NSLog(@"Firing synchronous url connection...");
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest
                                         returningResponse:&response
                                                     error:&error];
    if ([data length] > 0 &&
        error == nil){
        //NSLog(@"%lu bytes of data was returned.", (unsigned long)[data length]);
    }
    else if ([data length] == 0 &&
             error == nil){
        //NSLog(@"No data was returned.");
    }
    else if (error != nil){
        //NSLog(@"Error happened = %@", error);
    }
    //NSLog(@"We are done.");
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [Appirater userDidSignificantEvent:YES];
    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = YES;
    
    /*container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 110, 100)];
    activityLabel = [[UILabel alloc] init];
    activityLabel.text = NSLocalizedString(@"LOADING...", @"string1");
    activityLabel.textColor = [UIColor blackColor];
    activityLabel.font = [UIFont boldSystemFontOfSize:17];
    [container addSubview:activityLabel];
    
    activityLabel.frame = CGRectMake(155, 240,100, 25);
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [container addSubview:activityIndicator];
    activityIndicator.frame = CGRectMake(160, 200, 70, 25);
    
    [self.view addSubview:container];
    container.center = CGPointMake(frame.size.width/2, frame.size.height/2);
    */
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (self.tabBarController.selectedIndex == 3) {
        [self displayButtons];
        UIApplication* app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = NO;
//        activityLabel.hidden = TRUE;
//        [activityIndicator stopAnimating];
        
        @try {
            listOfItems = [[NSMutableArray alloc] init];
            helperMethods *myInstance = [[helperMethods alloc] init];
            NSArray *savedjobs = [myInstance fetchCoreDataJobs:@"Recentjobs"];
            NSString *string = self.savedJobsPosition;
            int value = [string intValue];
            NSManagedObject *info = [savedjobs objectAtIndex:value];
            NSArray *arrPosition = [NSArray arrayWithObject:[info valueForKey:@"jobname"]];
            self.position = [arrPosition objectAtIndex:0];
            NSDictionary *dictPosition = [NSDictionary dictionaryWithObject:arrPosition forKey:@"data"];
            
            NSArray *arrCity = [NSArray arrayWithObject:[info valueForKey:@"city"]];
            NSDictionary *dictCity = [NSDictionary dictionaryWithObject:arrCity forKey:@"data"];
            
            [listOfItems addObject:dictPosition];
            [listOfItems addObject:dictCity];
            
            NSArray *arrEmail = [NSArray arrayWithObject:[info valueForKey:@"email"]];
            NSDictionary *dictEmail = [NSDictionary dictionaryWithObject:arrEmail forKey:@"data"];
            [listOfItems addObject:dictEmail];
            
            NSArray *arrCompensation = [NSArray arrayWithObject:[info valueForKey:@"compensation"]];
            NSDictionary *dictCompensation = [NSDictionary dictionaryWithObject:arrCompensation forKey:@"data"];
            [listOfItems addObject:dictCompensation];
            
            
            NSArray *arrDescription = [NSArray arrayWithObject:[info valueForKey:@"desc"]];
            NSDictionary *dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
            [listOfItems addObject:dictDesc];
            
            NSArray *arrUrl = [NSArray arrayWithObject:[info valueForKey:@"url"]];
            NSDictionary *dictUrl = [NSDictionary dictionaryWithObject:arrUrl forKey:@"data"];
            [listOfItems addObject:dictUrl];
            
            lenDesc = [[info valueForKey:@"desc"] length];
            
        }
        @catch (NSException *exception) {
            NSLog(@"exception displaying elements from save");
        }

    }else if(self.fromWhere == @"mostviewed")
    {
        [self displayButtons];            
            self.descTextView.text = self.description;
            self.posLabel.text = self.position;
            
            //NSString *tmpTablename = self.tablename;
            NSString *tmpPostingid = self.postingid;
            g_url = self.url;
            
            //NSString *tmpParams = [@"?posting_id=" stringByAppendingFormat:tmpMutablePostingId];
            NSString *tmpURLCity = [@"&mostvieweddesc=" stringByAppendingString:@"yes"];
            NSString *tmpParams = [@"?posting_id=" stringByAppendingString:tmpPostingid];
            tmpParams = [tmpParams stringByAppendingString:tmpURLCity];
            //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/getMostViewed.php";
            NSString *urlAsString = @"http://69.194.193.163/cgj/getMostViewed.php";
            urlAsString = [urlAsString stringByAppendingString:tmpParams];
            g_reloadUrlString = urlAsString;
            NSURLRequest *theRequest = 
            [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                             cachePolicy:NSURLRequestReloadRevalidatingCacheData
                         timeoutInterval:30.0];
            
            // create the connection with the request
            // and start loading the data
            // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
            NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
            
            if (theConnection) {
                
                // Create the NSMutableData to hold the received data.
                // receivedData is an instance variable declared elsewhere.
                
                self.responseData = [NSMutableData data];
                listOfItems = [[NSMutableArray alloc] init];
                
                NSArray *arrPosition = [NSArray arrayWithObject:self.position];
                NSDictionary *dictPosition = [NSDictionary dictionaryWithObject:arrPosition forKey:@"data"];
                
                NSArray *arrCity = [NSArray arrayWithObject:self.city];
                NSDictionary *dictCity = [NSDictionary dictionaryWithObject:arrCity forKey:@"data"];
                
                NSArray *arrDescription = [NSArray arrayWithObject:self.description];
                NSDictionary *dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
                
                [listOfItems addObject:dictPosition];
                [listOfItems addObject:dictCity];
                [listOfItems addObject:dictDesc]; // this is for email. add it later
                [listOfItems addObject:dictDesc];
                [listOfItems addObject:dictDesc];
                
                
            } else {
                
                UIApplication* app = [UIApplication sharedApplication];
                app.networkActivityIndicatorVisible = NO;
                
                // Inform the user that the connection failed.
                //activityLabel.hidden = TRUE;
                //[activityIndicator stopAnimating];
                
                UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [connectFailMessage show];
                
            }   

    }else if (self.fromWhere != @"saved") {
        
        [self displayButtons];
        
        self.descTextView.text = self.description;
        self.posLabel.text = self.position;
        
        NSString *tmpTablename = self.tablename;
        //NSString *tmpPostingid = self.postingid;
        NSString *tmpPostingid = [NSString stringWithFormat:@"%@", self.postingid];
        g_url = self.url;
        //NSLog(@"posting id %@",teststring);
        //NSString *tmpParams = [@"?posting_id=" stringByAppendingFormat:tmpMutablePostingId];
        NSString *tmpURLCity = [@"&tablename=" stringByAppendingString:tmpTablename];
        NSString *tmpParams = [@"?posting_id=" stringByAppendingString:tmpPostingid];
        tmpParams = [tmpParams stringByAppendingString:tmpURLCity];
        //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/getDescViaPostingId.php";
        NSString *urlAsString = @"http://69.194.193.163/cgj/getDescViaPostingId.php";
        urlAsString = [urlAsString stringByAppendingString:tmpParams];
        g_reloadUrlString = urlAsString;
        // Create the request.
        NSURLRequest *theRequest = 
        [NSURLRequest requestWithURL:[NSURL URLWithString:urlAsString]
                         cachePolicy:NSURLRequestReloadRevalidatingCacheData
                     timeoutInterval:30.0];
        
        // create the connection with the request
        // and start loading the data
        // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            
            self.responseData = [NSMutableData data];
            listOfItems = [[NSMutableArray alloc] init];
            
            NSArray *arrPosition = [NSArray arrayWithObject:self.position];
            NSDictionary *dictPosition = [NSDictionary dictionaryWithObject:arrPosition forKey:@"data"];
            
            NSArray *arrCity = [NSArray arrayWithObject:self.city];
            NSDictionary *dictCity = [NSDictionary dictionaryWithObject:arrCity forKey:@"data"];
            
            NSArray *arrDescription = [NSArray arrayWithObject:self.description];
            NSDictionary *dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
            
            [listOfItems addObject:dictPosition];
            [listOfItems addObject:dictCity];
            [listOfItems addObject:dictDesc]; // this is for email. add it later
            [listOfItems addObject:dictDesc];
            [listOfItems addObject:dictDesc];
            
            
        } else {
            
            UIApplication* app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = NO;
            // Inform the user that the connection failed.
            //activityLabel.hidden = TRUE;
            //[activityIndicator stopAnimating];
            
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
            
        }   
    }else{
        [self displayButtons]; 
        UIApplication* app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = NO;
        
        //activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];
        @try {
            listOfItems = [[NSMutableArray alloc] init];
            helperMethods *myInstance = [[helperMethods alloc] init];
            NSArray *savedjobs = [myInstance fetchCoreDataJobs:@"Savedjobs"];
            NSString *string = self.savedJobsPosition;
            int value = [string intValue];
            NSManagedObject *info = [savedjobs objectAtIndex:value];
            NSArray *arrPosition = [NSArray arrayWithObject:[info valueForKey:@"jobname"]];
            self.position = [arrPosition objectAtIndex:0];
            NSDictionary *dictPosition = [NSDictionary dictionaryWithObject:arrPosition forKey:@"data"];
            
            NSArray *arrCity = [NSArray arrayWithObject:[info valueForKey:@"city"]];
            NSDictionary *dictCity = [NSDictionary dictionaryWithObject:arrCity forKey:@"data"];
            
            [listOfItems addObject:dictPosition];
            [listOfItems addObject:dictCity];
            
            NSArray *arrEmail = [NSArray arrayWithObject:[info valueForKey:@"email"]];
            NSDictionary *dictEmail = [NSDictionary dictionaryWithObject:arrEmail forKey:@"data"];
            [listOfItems addObject:dictEmail];
            
            NSArray *arrCompensation = [NSArray arrayWithObject:[info valueForKey:@"compensation"]];
            NSDictionary *dictCompensation = [NSDictionary dictionaryWithObject:arrCompensation forKey:@"data"];
            [listOfItems addObject:dictCompensation];

            
            NSArray *arrDescription = [NSArray arrayWithObject:[info valueForKey:@"desc"]];
            NSDictionary *dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
            [listOfItems addObject:dictDesc];
            
            // add dummy url for safari opening
            // else it will crash
           arrDescription = [NSArray arrayWithObject:@""];
           dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
            [listOfItems addObject:dictDesc];
            lenDesc = [[info valueForKey:@"desc"] length];

        }
        @catch (NSException *exception) {
            NSLog(@"exception displaying elements from save");
        }
    }
}

- (void)viewDidUnload
{
    [self setPosLabel:nil];
    [self setDescTextView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    @try {
        
    static NSString *CellIdentifier = @"SearchCell";
    UIWebView *webViewCell = nil; 
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        if(indexPath.section == 4)
        {
            //NSLog(@"index path outside desc ** => %d",indexPath.row);
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle =  UITableViewCellSelectionStyleNone;
            
            //webViewCell = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 300, 120)]; 
            webViewCell = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 310, 10000)]; 
            webViewCell.tag = 8080;
            [cell addSubview:webViewCell];

        }else
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            //cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier];
            cell.textLabel.textAlignment = UITextAlignmentLeft;

        }
    }
    
        NSDictionary *dictionary = [listOfItems objectAtIndex:indexPath.section];
        NSArray *array = [dictionary objectForKey:@"data"];
        NSString *cellValue = [array objectAtIndex:indexPath.row];
        
    if (indexPath.section == 4) {
        @try {
            //NSString *htmlval = @"<html><body>hi</body></html>";
            NSString *htmlval1 = @"<html><body>";
            NSString *htmlval2 = @"</body></html>";
            NSString *test = [htmlval1 stringByAppendingString:cellValue];
            NSString *finalStr = [test stringByAppendingString:htmlval2];
            webViewCell = (UIWebView*)[cell viewWithTag:8080];  
            webViewCell.userInteractionEnabled = YES;
            //NSLog(@"index path outside desc => %d",indexPath.row);
            if([finalStr length] < 5)
                finalStr = @"Please try opening this job opening in the browser, with the options above";
            [webViewCell loadHTMLString:finalStr baseURL:nil]; 
        }
        @catch (NSException *exception) {
            NSLog(@"Exception in webview displaying description: %@", exception);
            [webViewCell loadHTMLString:@"<html><body>Error loading description.</body></html>" baseURL:nil];
        }

    }else
    { 
        NSString *tmpcellvalue = cellValue;
        NSString *capitalisedSentence=@"";
        if ([tmpcellvalue length] > 0) {
            capitalisedSentence = 
            [tmpcellvalue stringByReplacingCharactersInRange:NSMakeRange(0,1)  
                                                withString:[[tmpcellvalue  substringToIndex:1] capitalizedString]];
        }
        
        cell.textLabel.text=capitalisedSentence;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.textLabel.numberOfLines = 2; 

    }
    return cell;
    }@catch (NSException *exception) {
                NSLog(@"Exception in webview displaying description: %@", exception);
            }    
    
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)];
    NSString *tmptitle=@"";
    if(section == 0)
        tmptitle= @"Title";
    else if(section == 1)
        tmptitle= @"City";
    else if(section == 2)
        tmptitle= @"Email";
    else if(section == 3)
        tmptitle= @"Compensation";
    else
        tmptitle= @"Description";
    label.text = tmptitle;
    label.font = [UIFont boldSystemFontOfSize:16];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor colorWithRed:54/256.0 green:100/256.0 blue:139/256.0 alpha:1.0];
    [headerView addSubview:label];
    [headerView setBackgroundColor:[UIColor colorWithRed:54/256.0 green:100/256.0 blue:139/256.0 alpha:1.0]];
    return headerView;
}

- (CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat result = 40.0f;
    if(indexPath.section == 4) {
        
        // number of chars in a line approx 36
        int tmpVal = lenDesc;
        int tmpRes = tmpVal / 36;
        CGFloat tmpFloatVal = tmpRes * 100;
        //NSLog(@"%d",tmpRes * 100);
        if (tmpFloatVal < 50.0f) {
            return 50.0f;
        }
        return tmpFloatVal;
    }
    return result;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if(section == 0)
        return @"Title";
    else if(section == 1)
        return @"City";
    else if(section == 2)
        return @"Email";
    else if(section == 3)
        return @"Compensation";
    else
        return @"Description";
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    //[tableView beginUpdates];
    //[tableView endUpdates];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if ([data length] > 0){
        //NSLog(@"%lu bytes of data was returned.", (unsigned long)[data length]);
    }else
    {
        UIApplication* app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = NO;
        //activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Error receiving data from server due to internet connectivity / No data available. Please check back again."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {

    UIApplication* app = [UIApplication sharedApplication];
	app.networkActivityIndicatorVisible = NO;
    
    //activityLabel.hidden = TRUE;
    //[activityIndicator stopAnimating];
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"Sorry.No Internet Connection/Request timed out. Please try again."
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
    [alertView show];
    
    
}

- (void) loadDescriptionTableAgain:(NSString*)p_reloadUrlString
{
    NSString *urlAsString = p_reloadUrlString;
    NSURL *tmp_url = [NSURL URLWithString:urlAsString];
    //NSLog(urlAsString);
    NSURLRequest *urlRequest =
    [NSURLRequest
     requestWithURL:tmp_url
     cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
     timeoutInterval:30.0f];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 &&
             error == nil){
             
             NSString *html = [[NSString alloc] initWithData:data
                                                    encoding:NSUTF8StringEncoding];
             UIApplication* app = [UIApplication sharedApplication];
             app.networkActivityIndicatorVisible = NO;
             //NSLog(@"HTML = %@", html);
             
             [listOfItems removeAllObjects];
             //[NSThread sleepForTimeInterval:1.0];
             NSArray *arrPosition = [NSArray arrayWithObject:self.position];
             NSDictionary *dictPosition = [NSDictionary dictionaryWithObject:arrPosition forKey:@"data"];
             
             NSArray *arrCity = [NSArray arrayWithObject:self.city];
             NSDictionary *dictCity = [NSDictionary dictionaryWithObject:arrCity forKey:@"data"];
             
             [listOfItems addObject:dictPosition];
             [listOfItems addObject:dictCity];
             
             NSString *responseEmail;
             NSString *responseComp;
             
             NSArray *userData = [html JSONValue];
             
             for (NSDictionary *user in userData) {
                 responseEmail = [user objectForKey:@"email"];
                 responseComp = [user objectForKey:@"comp"];
             }
             
             NSArray *arrEmail = [NSArray arrayWithObject:responseEmail];
             NSDictionary *dictEmail = [NSDictionary dictionaryWithObject:arrEmail forKey:@"data"];
             
             NSArray *arrComp = [NSArray arrayWithObject:responseComp];
             NSDictionary *dictComp = [NSDictionary dictionaryWithObject:arrComp forKey:@"data"];
             
             NSString *responseDesc;
             NSArray *arrDescription;
             NSDictionary *dictDesc;
             
             for (NSDictionary *user in userData) {
                 if([[user objectForKey:@"desc"] isKindOfClass:[NSString class]])
                 {
                     responseDesc = [user objectForKey:@"desc"];
                 }else
                     responseDesc = @"";
             }
             @try {
                 
                 if([responseDesc isKindOfClass:[NSString class]])
                 {
                     if (responseDesc != NULL ) {
                         lenDesc = [responseDesc length];
                     }
                 }else
                     lenDesc = 100;
             }
             @catch (NSException * e) {
                 NSLog(@"Exception: %@", e);
                 lenDesc = 100;
             }
             
             [listOfItems addObject:dictEmail];
             [listOfItems addObject:dictComp];
             
             arrDescription = [NSArray arrayWithObject:responseDesc];
             dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
             [listOfItems addObject:dictDesc];
         }
         else if ([data length] == 0 &&
                  error == nil){
             UIApplication* app = [UIApplication sharedApplication];
             app.networkActivityIndicatorVisible = NO;
             //NSLog(@"Nothing was downloaded.");
         }
         else if (error != nil){
             UIApplication* app = [UIApplication sharedApplication];
             app.networkActivityIndicatorVisible = NO;
             //NSLog(@"Error happened = %@", error);
         }
     }];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    //NSLog(@"finished loading");
    
    NSString *responseString = [[NSString alloc] initWithData:responseData
                                                     encoding:NSUTF8StringEncoding];
    NSArray *userData = [responseString JSONValue];
    int elements = [userData count];
    if (elements < 1) {
        
        //activityLabel.hidden = TRUE;
        //[activityIndicator stopAnimating];
        
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Sorry !!! No description available for this job."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
        
    }else
    {
        
        [listOfItems removeAllObjects];
        //[NSThread sleepForTimeInterval:1.0];
        NSArray *arrPosition = [NSArray arrayWithObject:self.position];
        NSDictionary *dictPosition = [NSDictionary dictionaryWithObject:arrPosition forKey:@"data"];
        
        NSArray *arrCity = [NSArray arrayWithObject:self.city];
        NSDictionary *dictCity = [NSDictionary dictionaryWithObject:arrCity forKey:@"data"];
        
        [listOfItems addObject:dictPosition];
        [listOfItems addObject:dictCity];
        
        NSString *responseEmail;
        NSString *responseComp;
        for (NSDictionary *user in userData) {
            responseEmail = [user objectForKey:@"email"];
            responseComp = [user objectForKey:@"comp"];
        }
        
        NSArray *arrEmail = [NSArray arrayWithObject:responseEmail];
        NSDictionary *dictEmail = [NSDictionary dictionaryWithObject:arrEmail forKey:@"data"];
        
        NSArray *arrComp = [NSArray arrayWithObject:responseComp];
        NSDictionary *dictComp = [NSDictionary dictionaryWithObject:arrComp forKey:@"data"];
        
        NSString *responseDesc;
        NSArray *arrDescription;
        NSDictionary *dictDesc;
        for (NSDictionary *user in userData) {
            if([[user objectForKey:@"desc"] isKindOfClass:[NSString class]])
            {
                responseDesc = [user objectForKey:@"desc"];
            }else
                responseDesc = @"";
        }
        @try {
            
            if([responseDesc isKindOfClass:[NSString class]])
            {
                if (responseDesc != NULL ) {
                    lenDesc = [responseDesc length];
                }
            }else
                lenDesc = 100;
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
            lenDesc = 100;
        }

        [listOfItems addObject:dictEmail];
        [listOfItems addObject:dictComp];
        
        arrDescription = [NSArray arrayWithObject:responseDesc];
        dictDesc = [NSDictionary dictionaryWithObject:arrDescription forKey:@"data"];
        [listOfItems addObject:dictDesc];
        
        NSArray *arrUrl = [NSArray arrayWithObject:self.url];
        NSDictionary *dictUrl = [NSDictionary dictionaryWithObject:arrUrl forKey:@"data"];
        [listOfItems addObject:dictUrl];
        
        helperMethods *myInstance = [[helperMethods alloc] init];
        [myInstance saveDataCoreDataRecents:@"Recentjobs" :self.position :responseDesc :responseEmail :self.city :responseComp:self.url];  
        
        if([responseDesc length] < 10)
        {
            UIApplication* app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = YES;
            [self loadDescriptionTableAgain:g_reloadUrlString];
        }
        
        UIApplication* app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = NO;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self insertMostViewedJobs:self.postingid: self.position : self.url: responseDesc :responseEmail :self.city :responseComp];
            //NSLog(@"Done doing something long and involved");
        });
        
        
        [self.tableView reloadData];
        //[self performSelector:@selector(reloadSections) withObject:nil afterDelay:.2];
    } 
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *twtString = @"Apply for this position: \n";
    NSDictionary *dictionary;
    NSArray *array;
    NSString *desc;
    //UIAlertView *alert;
	switch (buttonIndex) {
        case 0:
            [self openInBrowser];
            break;
            
        case 1:
            
            /*alert = [[UIAlertView alloc]
                     initWithTitle: @"Question ???"
                     message: @"Do you want to attach the default resume ? \n The default resume can be attached from clicking on More - Resume from Dropbox and following the instructions specified. "
                     delegate: self
                     cancelButtonTitle:@"NO"
                     otherButtonTitles:@"YES",nil];
            
            [alert show];
            NSLog(defaultResumeString);*/
            [self applyForJobInEmail];
            break;
            
        case 2:
            [self saveJobForLater];
            break;
        
        case 3:
            [self openInEmail];
            break;
        case 4:
            dictionary = [listOfItems objectAtIndex:0];
            array = [dictionary objectForKey:@"data"];
            desc = [array objectAtIndex:0];
            
            twtString = [twtString stringByAppendingString:desc];
            
            dictionary = [listOfItems objectAtIndex:5];
            array = [dictionary objectForKey:@"data"];
            desc = [array objectAtIndex:0];
            
            twtString = [twtString stringByAppendingString:desc];
            self.twitterController = [[TWTweetComposeViewController alloc] init];
            [self.twitterController setInitialText:twtString];
            [self.navigationController presentModalViewController:self.twitterController
                                                         animated:YES];
            break;
        case 5:
            //NSLog(@"do nothing 4");
            [self.actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
            break;    
        default:
            break;
    }

}

- (IBAction)clickDisplaySendOptions:(id)sender {
    
    if ([self.actionSheet isVisible]) {
        [self.actionSheet dismissWithClickedButtonIndex:-1 animated:NO];
        
    } else {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            [self.actionSheet showFromBarButtonItem:self.forwardButton animated:NO];
            
        } else {
            //NSLog(@"last option **");
            //[self.actionSheet showInView:[self view]];
            [self.actionSheet showInView:self.parentViewController.tabBarController.view];
        }
	}
    /*
     sheet = [[UIActionSheet alloc] initWithTitle:@"More Info"
                                        delegate:self
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:nil
                               otherButtonTitles:@"Facebook", @"Twitter", 
             @"Open in Safari", @"Email job opening", nil];
    
    // Show the sheet
    [sheet showInView:self.view];*/
    
}


- (UIActionSheet *)actionSheet {
    
    if (_actionSheet == nil) {
        _actionSheet = [[UIActionSheet alloc] initWithTitle:@"More Info"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:@"Open in Safari", @"Apply via Email", 
                        @"Save / Bookmark", @"Email/Forward job opening", @"Tweet this job", nil];           
    }
    
    return _actionSheet;
}

- (void)openInBrowser {
    
    NSDictionary *dictionary;
    NSArray *array;
    NSString *desc;
    // get the email
    if([g_url length]<1)
    {
        dictionary = [listOfItems objectAtIndex:5];
        array = [dictionary objectForKey:@"data"];
        desc = [array objectAtIndex:0];

        NSURL *tmpurl = [NSURL URLWithString:desc];
        if (tmpurl) {
            [[UIApplication sharedApplication] openURL:tmpurl];
        }
        
    }else{
        NSURL *tmpurl = [NSURL URLWithString:g_url];
        if (tmpurl) {
            [[UIApplication sharedApplication] openURL:tmpurl];
        }   
    }
}

- (void)saveJobForLater {
    
    //NSLog(@"save job for later");
   
    NSString *tmpPosition = @"";
    NSString *tmpdesc = @"";
    NSString *email = @"";
    NSString *error = @"";
    NSString *tmpcity = @"";
    NSString *tmpcompensation = @"";
    @try{
        // get the email
        NSDictionary *dictionary = [listOfItems objectAtIndex:2];
        NSArray *array = [dictionary objectForKey:@"data"];
        email = [array objectAtIndex:0];
       
        // get the city
        dictionary = [listOfItems objectAtIndex:1];
        array = [dictionary objectForKey:@"data"];
        tmpcity = [array objectAtIndex:0];
        
        // get the job position
        dictionary = [listOfItems objectAtIndex:0];
        array = [dictionary objectForKey:@"data"];
        tmpPosition = [array objectAtIndex:0];
        
        // get the description
        dictionary = [listOfItems objectAtIndex:4];
        array = [dictionary objectForKey:@"data"];
        tmpdesc = [array objectAtIndex:0];
        
        dictionary = [listOfItems objectAtIndex:3];
        array = [dictionary objectForKey:@"data"];
        tmpcompensation = [array objectAtIndex:0];
        
        if ([email length] < 1)
            email = @"email...";
        if ([tmpPosition length] < 1)
            tmpPosition = @"position...";
        if ([tmpdesc length] < 1) {
            tmpdesc = @"desc...";
        }
        if ([tmpcity length] < 1) {
            tmpcity = @"city...";
        }
        if ([tmpcompensation length] < 1) {
            tmpcompensation = @"...";
        }
        helperMethods *myInstance = [[helperMethods alloc] init];
        [myInstance saveDataCoreData:tmpPosition :tmpdesc :email :tmpcity :tmpcompensation];  
        

    }@catch (NSException *exception) {
        error = @"yes";
        NSLog(@"exception saving jobs");
    }
    
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != [alertView cancelButtonIndex])
    {
        helperMethods *myInstance = [[helperMethods alloc] init];
        NSString *tmpvar = [myInstance getDefaultResume];
        defaultResumeString = tmpvar;
        
    }
}

- (void)applyForJobInEmail {
    
    helperMethods *myInstance = [[helperMethods alloc] init];
    NSString *tmpvar = [myInstance getDefaultResume];
    defaultResumeString = tmpvar;
    NSString *tmpvar2 = [myInstance getDefaultCover];
    NSString *tmpContent = [myInstance getDefaultEmailBody];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:defaultResumeString];
    NSString *filePathCover = [documentsDirectory stringByAppendingPathComponent:tmpvar2];
    NSString *filePathContent = [documentsDirectory stringByAppendingPathComponent:tmpContent];
    //NSLog(filePath);
    if ([MFMailComposeViewController canSendMail]) {
        
        // get the email
        NSDictionary *dictionary = [listOfItems objectAtIndex:2];
        NSArray *array = [dictionary objectForKey:@"data"];
        NSString *email = [array objectAtIndex:0];
        
        
        // get the job description
        dictionary = [listOfItems objectAtIndex:0];
        array = [dictionary objectForKey:@"data"];
        NSString *tmpPosition = [array objectAtIndex:0];
        
        tmpPosition = [@"Application for " stringByAppendingString:tmpPosition];
        
        MFMailComposeViewController *viewController = [[MFMailComposeViewController alloc] init];
        viewController.mailComposeDelegate = self;

        NSArray * recepients = [[NSArray alloc] initWithObjects:email,nil];
        [viewController setSubject:tmpPosition];
        [viewController setToRecipients:recepients];
        
        // read content from file
        //NSString *tmpContentStr;
        NSString* content = [NSString stringWithContentsOfFile:filePathContent
                                                      encoding:NSUTF8StringEncoding
                                                         error:NULL];
        if ([content length] > 0) {
                [viewController setMessageBody:content isHTML:YES]; 
        }else
                [viewController setMessageBody:@"" isHTML:YES];  
        
        /*
        NSString *path = [[NSBundle mainBundle] pathForResource:@"cities" ofType:@"plist"];
        NSData *myData = [NSData dataWithContentsOfFile:path];
        [viewController addAttachmentData:myData mimeType:@"text/plist" fileName:@"cities"];*/
        
        // start finding the document types
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        if(fileExists && [defaultResumeString length] > 0)
        {
            NSMutableData *myData;
            NSRange textRange;
            textRange =[[defaultResumeString lowercaseString] rangeOfString:[@".pdf" lowercaseString]];
            if(textRange.location != NSNotFound)
            {
                // pdf is found
                myData = [NSMutableData dataWithContentsOfFile:filePath];
                [viewController addAttachmentData:myData mimeType:@"application/pdf" fileName:defaultResumeString];
            }
            textRange =[[defaultResumeString lowercaseString] rangeOfString:[@".txt" lowercaseString]];
            if(textRange.location != NSNotFound)
            {
                // text file
                myData = [NSMutableData dataWithContentsOfFile:filePath];
                [viewController addAttachmentData:myData mimeType:@"txt" fileName:defaultResumeString];
            }
            textRange =[[defaultResumeString lowercaseString] rangeOfString:[@".doc" lowercaseString]];
            if(textRange.location != NSNotFound)
            {
                // doc is found
                myData = [NSMutableData dataWithContentsOfFile:filePath];
                [viewController addAttachmentData:myData mimeType:@"application/doc" fileName:defaultResumeString];
            }  
            textRange =[[defaultResumeString lowercaseString] rangeOfString:[@".docx" lowercaseString]];
            if(textRange.location != NSNotFound)
            {
                // doc is found
                myData = [NSMutableData dataWithContentsOfFile:filePath];
                [viewController addAttachmentData:myData mimeType:@"application/docx" fileName:defaultResumeString];
            }  
        }
        
        BOOL fileExistsCover = [[NSFileManager defaultManager] fileExistsAtPath:filePathCover];
        if(fileExistsCover && [filePathCover length] > 0)
        {
            NSMutableData *myData;
            NSRange textRange;
            textRange =[[filePathCover lowercaseString] rangeOfString:[@".pdf" lowercaseString]];
            if(textRange.location != NSNotFound)
            {
                // pdf is found
                myData = [NSMutableData dataWithContentsOfFile:filePath];
                [viewController addAttachmentData:myData mimeType:@"application/pdf" fileName:tmpvar2];
            }
            textRange =[[filePathCover lowercaseString] rangeOfString:[@".txt" lowercaseString]];
            if(textRange.location != NSNotFound)
            {
                // text file
                myData = [NSMutableData dataWithContentsOfFile:filePath];
                [viewController addAttachmentData:myData mimeType:@"txt" fileName:tmpvar2];
            }
            textRange =[[filePathCover lowercaseString] rangeOfString:[@".doc" lowercaseString]];
            if(textRange.location != NSNotFound)
            {
                // doc is found
                myData = [NSMutableData dataWithContentsOfFile:filePath];
                [viewController addAttachmentData:myData mimeType:@"application/doc" fileName:tmpvar2];
            }  
            textRange =[[filePathCover lowercaseString] rangeOfString:[@".docx" lowercaseString]];
            if(textRange.location != NSNotFound)
            {
                // doc is found
                myData = [NSMutableData dataWithContentsOfFile:filePath];
                [viewController addAttachmentData:myData mimeType:@"application/docx" fileName:tmpvar2];
            }  
        }
        if(fileExists == FALSE && fileExistsCover == FALSE)
        {
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Information" message:@"You do not have any resume OR cover letter attached in your email. \nIf you want to attach resumes/cover letter and apply for jobs, \n1.click on more..\n2.Resume from Dropbox.\n3.Download resumes from dropbox.\n4.select a default resume.\nIt will be automatically downloaded to your phone and attached to your application."  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];

        }
        [self presentModalViewController:viewController animated:YES];
        //[viewController release];
    }
}

- (void)openInEmail {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        // get the description
        NSDictionary *dictionary = [listOfItems objectAtIndex:4];
        NSArray *array = [dictionary objectForKey:@"data"];
        NSString *desc = [array objectAtIndex:0];
        
        // get the email
        dictionary = [listOfItems objectAtIndex:2];
        array = [dictionary objectForKey:@"data"];
        NSString *email = [array objectAtIndex:0];

        
        // get the job description
        dictionary = [listOfItems objectAtIndex:0];
        array = [dictionary objectForKey:@"data"];
        NSString *tmpPosition = [array objectAtIndex:0];
        
        NSString *tmpdesc = [@"<b>Email: </b>" stringByAppendingString:email];
        tmpdesc = [tmpdesc stringByAppendingString:@"<br><br> <b>Description:</b><br>"];
        desc = [tmpdesc stringByAppendingString:desc];

        MFMailComposeViewController *viewController = [[MFMailComposeViewController alloc] init];
        viewController.mailComposeDelegate = self;
        
        [viewController setSubject:tmpPosition];
        [viewController setMessageBody:desc isHTML:YES];      
        [self presentModalViewController:viewController animated:YES];
        //[viewController release];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	[self dismissModalViewControllerAnimated:YES];
}

@end
