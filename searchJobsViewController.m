//
//  searchJobsViewController.m
//  Craigslist JOBS
//
//  Created by darshan katrumane on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "searchJobsViewController.h"
#import "searchResultsTableViewController.h"
#import "helperMethods.h"
#import "recentSearchesTableViewController.h"
#import "searchSelectCityOrDeptTableView.h"
#import "dataclass.h"
@implementation searchJobsViewController
@synthesize jobnameTextFieldReturn;
@synthesize deptPicker;
@synthesize cityNames;
@synthesize cityPickerOutlet;
@synthesize picker;
@synthesize deptNames;
@synthesize keywordTextFieldReturn;
@synthesize cityTableView;
@synthesize jobtypeTableView;
@synthesize cityLabel;
@synthesize cities;
@synthesize jobtypeLabel;
@synthesize cityTextfield;
@synthesize deptTextfield;
@synthesize tableSearchJobs;
@synthesize addButtonCity;
@synthesize addButtonDept;
@synthesize setterCityField;
@synthesize setterDeptField;
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (IBAction)showCityTable:(id)sender {
    
    searchSelectCityOrDeptTableView *viewController = [searchSelectCityOrDeptTableView alloc];
    viewController.selectedid = @"city";
    [self presentModalViewController:viewController animated:YES];
    //cityTableView.hidden = NO;
    //jobtypeTableView.hidden = YES;
    
}

/*
- (IBAction)selectCity:(id)sender {
    //NSLog(@"selectCity");
    displayCitiesForSearch *displayCitiesController = [displayCitiesForSearch alloc];
    [self.navigationController pushViewController:displayCitiesController animated:YES];
    
}*/

-(IBAction)backgroundTouchedKeywords:(id)sender
{
    [jobnameTextFieldReturn resignFirstResponder];
    [keywordTextFieldReturn resignFirstResponder];
}

-(IBAction)backgroundTouched:(id)sender
{
    [jobnameTextFieldReturn resignFirstResponder];
    cityTableView.hidden = YES;
    jobtypeTableView.hidden = YES;
}
-(IBAction)jobNameActiontextFieldReturn:(id)sender
{
     [sender resignFirstResponder];
}
- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return [cityNames count];
    }
    return [deptNames count];
}

/*
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if (component == 0) {
		return [cityNames objectAtIndex:row];
	}
	else {
		return [deptNames objectAtIndex:row];
	}
}*/

- (NSString *) tableView:(UITableView *)tableView
 titleForHeaderInSection:(NSInteger)section{
    NSString *result = nil;
    if ([tableView isEqual:self.tableSearchJobs] &&
        section == 0){
        result = @"Position / Job Title";
    }
    if ([tableView isEqual:self.tableSearchJobs] &&
        section == 1){
        result = @"Job type / Department";
    }
    
    if ([tableView isEqual:self.tableSearchJobs] &&
        section == 2){
        result = @"City / County";
    }
    return result;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *retval = (id)view;
    if (!retval) {
        retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
        
        [retval setFont:[UIFont fontWithName:@"Times New Roman" size:14.0]];
    }
    if (component == 0) {
		retval.text = [cityNames objectAtIndex:row];
	}
	else {
		retval.text = [deptNames objectAtIndex:row];
	}
    retval.font = [UIFont systemFontOfSize:14];
    return retval;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    if(tableView == self.tableSearchJobs)
    {
        return 1;
    }else if (tableView == self.jobtypeTableView) {
        return  [deptNames count];
    }
    return [cityNames count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField;
{
    jobnameTextFieldReturn = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        
        if(tableView == self.tableSearchJobs)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            if(indexPath.row == 0 && indexPath.section == 0)
            {
                CGRect textRect = CGRectMake(cell.frame.origin.x+10, cell.frame.origin.y+10, 270.0f, tableView.rowHeight+5);
                
                UITextField *myfield = [[UITextField alloc]initWithFrame:textRect];
                myfield.borderStyle = UITextBorderStyleNone;
                myfield.font = [UIFont boldSystemFontOfSize:15.0];
                myfield.adjustsFontSizeToFitWidth = YES;
                myfield.minimumFontSize = 2.0;
                myfield.placeholder = @"";
                
                //myfield.clearButtonMode = UITextFieldViewModeWhileEditing;
                //myfield.keyboardType = UIKeyboardTypeDefault;
                //myfield.autocorrectionType= UITextAutocorrectionTypeNo;
                //myfield.autocapitalizationType=UITextAutocapitalizationTypeNone;
                //myfield.returnKeyType=UIReturnKeyDefault;  
                jobnameTextFieldReturn = myfield;
                jobnameTextFieldReturn.delegate = self;
                [cell.contentView addSubview: jobnameTextFieldReturn];
            }
            
            if(indexPath.row == 0 && indexPath.section == 1)
            {
                CGRect textRect = CGRectMake(cell.frame.origin.x+10, cell.frame.origin.y+10, 270.0f, tableView.rowHeight+5);
                UITextField *myfield = [[UITextField alloc]initWithFrame:textRect];
                myfield.borderStyle = UITextBorderStyleNone;
                myfield.font = [UIFont boldSystemFontOfSize:15.0];
                myfield.adjustsFontSizeToFitWidth = YES;
                myfield.minimumFontSize = 2.0;
                myfield.placeholder=@"";
                myfield.clearButtonMode = UITextFieldViewModeWhileEditing;
                myfield.keyboardType = UIKeyboardTypeDefault;
                myfield.autocorrectionType= UITextAutocorrectionTypeNo;
                myfield.autocapitalizationType=UITextAutocapitalizationTypeNone;
                myfield.returnKeyType=UIReturnKeyDone;
                myfield.enabled=false;
                myfield.selected=true;
                
                deptTextfield = myfield;
                [cell.contentView addSubview: deptTextfield];
                
                addButtonDept = [UIButton buttonWithType:UIButtonTypeContactAdd];
                addButtonDept.frame = CGRectMake(260, cell.frame.origin.y+6, 40, 30); // position in the parent view and set the size of the button
                //[addButtonDept setTitle:@"Registry now" forState:UIControlStateNormal];
                // add targets and actions
                [addButtonDept addTarget:self action:@selector(jobtypeButton:) forControlEvents:UIControlEventTouchUpInside];   
                [cell.contentView addSubview: addButtonDept];
            }
            
            if(indexPath.row == 0 && indexPath.section == 2)
            {
                CGRect textRect = CGRectMake(cell.frame.origin.x+10, cell.frame.origin.y+10, 270.0f, tableView.rowHeight+5);
                UITextField *myfield = [[UITextField alloc]initWithFrame:textRect];
                myfield.borderStyle = UITextBorderStyleNone;
                myfield.font = [UIFont boldSystemFontOfSize:15.0];
                myfield.adjustsFontSizeToFitWidth = YES;
                myfield.minimumFontSize = 2.0;
                
                myfield.clearButtonMode = UITextFieldViewModeWhileEditing;
                myfield.keyboardType = UIKeyboardTypeDefault;
                myfield.autocorrectionType= UITextAutocorrectionTypeNo;
                myfield.autocapitalizationType=UITextAutocapitalizationTypeNone;
                myfield.returnKeyType=UIReturnKeyDone;
                myfield.enabled=false;
                myfield.placeholder=@"";
                                
                cityTextfield = myfield;
                [cell.contentView addSubview: cityTextfield];
                
                addButtonCity = [UIButton buttonWithType:UIButtonTypeContactAdd];
                addButtonCity.frame = CGRectMake(260, cell.frame.origin.y+6, 40, 30);
                [addButtonCity addTarget:self action:@selector(showCityTable:) forControlEvents:UIControlEventTouchUpInside];   
                [cell.contentView addSubview: addButtonCity];
            }
            // add to a view            
            
            
            /*
             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
             UITextView *comment = [[UITextView alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 40.0f, tableView.rowHeight)];
             comment.editable = YES;
             comment.delegate = self;
             comment.layer.borderWidth = 1.0f;
             comment.layer.borderColor = [[UIColor grayColor] CGColor];
             NSLog(@"in text");
             [cell.contentView addSubview:comment];*/
        }else
        {
        cell = [[UITableViewCell alloc] 
                 initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
        }
    }
    
    if (tableView == self.jobtypeTableView) {
        cell.textLabel.text = [deptNames objectAtIndex:indexPath.row];
        // cell.textLabel.font = [UIFont fontWithName:@"ArialMT" size:17];
        cell.textLabel.font = [UIFont fontWithName:@"Verdana-BoldItalic" size:14];
    }
    if(tableView == self.cityTableView){
        cell.textLabel.text = [cityNames objectAtIndex:indexPath.row];
        //cell.textLabel.font = [UIFont fontWithName:@"ArialMT" size:17];
        cell.textLabel.font = [UIFont fontWithName:@"Verdana-BoldItalic" size:14];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [jobnameTextFieldReturn resignFirstResponder];
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *tmpStr = selectedCell.textLabel.text;
    UIView *myBackView = [[UIView alloc] initWithFrame:selectedCell.frame];
    myBackView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.75 alpha:1];
    selectedCell.selectedBackgroundView = myBackView;
    if(tableView == self.cityTableView)
    {
        cityLabel.text = tmpStr;
        cityTextfield.text = tmpStr;
        cityTableView.hidden = YES;   
    }
    if (tableView == self.jobtypeTableView) {
        jobtypeLabel.text = tmpStr;
        deptTextfield.text = tmpStr;
        jobtypeTableView.hidden = YES;
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    helperMethods *myInstance = [helperMethods alloc];
    //NSLog(@"selected value %@", [myInstance getDefaultSelectedSearchValue:@"SelectedCity"]);
    deptTextfield.text = [myInstance getDefaultSelectedSearchValue:@"SelectedDepartment"];
    cityTextfield.text = [myInstance getDefaultSelectedSearchValue:@"SelectedCity"];
}

- (CGFloat) tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section{
    
    CGFloat result = 0.0f;
    //    if (section == 0){
    //      result = 30.0f;
    // }
    return result;
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:19/256.0 green:133/256.0 blue:45/256.0 alpha:1.0];
    
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath   = [mainBundle pathForResource:@"jobtype" ofType:@"plist"];
    self.deptNames = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    
    mainBundle = [NSBundle mainBundle];
    filePath   = [mainBundle pathForResource:@"cities" ofType:@"plist"];
    self.cityNames = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    
}

- (void)viewDidUnload
{
    [self setCityPickerOutlet:nil];
    [self setDeptPicker:nil];
    [self setJobnameTextFieldReturn:nil];
    [self setTableSearchJobs:nil];
    helperMethods *myinstance = [helperMethods alloc];
    [myinstance emptyTable:@"SelectedCity"];
    [myinstance emptyTable:@"SelectedDepartment"];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)searchJobsAction:(id)sender {
    recentSearchesTableViewController *searchController = [recentSearchesTableViewController alloc];
    [self.navigationController pushViewController:searchController animated:YES]; 

}
- (IBAction)touchInsideKeywords:(id)sender {
    self.jobnameTextFieldReturn.enabled = false;
    self.jobnameTextFieldReturn.text = @"";
}
- (IBAction)editingChangedPositions:(id)sender {
    self.jobnameTextFieldReturn.enabled = true;
    self.keywordTextFieldReturn.enabled = false;
    self.keywordTextFieldReturn.text = @"";
}
-(void) successNotification
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Information"
                              message:@"Awesome!!! You have successfully registered for notifications. We will tell you when a new job shows up."
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
    [alertView show];
}
- (void) insertUdid:(NSString *)udid:(NSString *)table:(NSString*)city:(NSString *)keywords
{
    //[self successNotification ];
    
    NSString *tmpParams = [@"udid=" stringByAppendingString:udid];
    NSString *tmpposition = [@"&table=" stringByAppendingString:table];
    tmpParams = [tmpParams stringByAppendingString:tmpposition];
    NSString *tmpVar = [@"&city=" stringByAppendingString:city];
    tmpParams = [tmpParams stringByAppendingString:tmpVar];
    tmpVar = [@"&keywords=" stringByAppendingString:keywords];
    tmpParams = [tmpParams stringByAppendingString:tmpVar];
    
    
    //NSString *urlAsString = @"http://127.0.0.1/~spycar/cgjobs/insertUniqueIdKeywords.php";
    NSString *urlAsString = @"http://69.194.193.163/cgj/insertUniqueIdKeywords.php";
    NSLog(@"%@",urlAsString);
    
    NSURL *tmp_url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:tmp_url];
    [urlRequest setTimeoutInterval:50.0f];
    [urlRequest setHTTPMethod:@"POST"];
    NSLog(@"%@", tmpParams);
    [urlRequest setHTTPBody:[tmpParams dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLResponse *response = nil;
    NSError *error = nil;
    //NSLog(@"Firing synchronous url connection...");
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest
                                         returningResponse:&response
                                                     error:&error];
    
    if ([data length] > 0 &&
        error == nil){
    }
    else if ([data length] == 0 &&
             error == nil){
        //NSLog(@"No data was returned.");
    }
    else if (error != nil){
        //NSLog(@"Error happened = %@", error);
    }
    //NSLog(@"We are done.");
}
- (void)addUniqueDeviceId:(NSNotification *)notif {
    
    // get the data from the obj
	DataClass *obj=[DataClass getInstance]; 
    
    helperMethods *myInstance = [[helperMethods alloc] init];
    NSString *tmpDescTableName = [myInstance returnDatabaseTableDescName:self.deptTextfield.text];
    NSString *tmpCityname = self.cityTextfield.text;
    NSString *tmpKeywords = self.jobnameTextFieldReturn.text;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self insertUdid: obj.str: tmpDescTableName : tmpCityname: tmpKeywords];    });
}

-(IBAction)notifyButtonClicked:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Information"
                              message:@"Extremely sorry about this. We recently came to know about an issue in sending notifications. We are working on it."
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
    [alertView show];

    /*
    searchResultsTableViewController *searchController = [searchResultsTableViewController alloc];
    if( [deptTextfield.text length] == 0 ||
       [cityTextfield.text length] == 0 ||
       [jobnameTextFieldReturn.text length] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Please fill in the fields you want notifications to be sent."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
        
    }else
    {
        helperMethods *myInstance = [[helperMethods alloc] init];
        [myInstance saveDataSearch:self.jobnameTextFieldReturn.text :self.deptTextfield.text :self.cityTextfield.text]; 
        searchController.dept = deptTextfield.text;
        searchController.city = cityTextfield.text;
        searchController.keywords = @"keywords"; // need to change this
        searchController.jobname = self.jobnameTextFieldReturn.text;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        [self successNotification];
        // add the notification
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addUniqueDeviceId:) 
                                                     name:@"com.redflower.search.uniquedeviceid" object:nil];
    }*/
    
}
- (IBAction)segmentControlClick:(id)sender {
    //NSLog(@"segment %d", segmentedControl.selectedSegmentIndex);
    if (segmentedControl.selectedSegmentIndex == 1) {
        
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Search is not yet optimized for keyword based search."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
        [segmentedControl setSelectedSegmentIndex:0];
    }
}

- (IBAction)jobtypeButton:(id)sender {
    [jobnameTextFieldReturn resignFirstResponder];
    searchSelectCityOrDeptTableView *viewController = [searchSelectCityOrDeptTableView alloc];
    viewController.selectedid = @"dept";
    [self presentModalViewController:viewController animated:YES];
    /*jobtypeTableView.hidden=NO;
    cityTableView.hidden=YES;*/
}
- (IBAction)searchjobsbutton:(id)sender {
    searchResultsTableViewController *searchController = [searchResultsTableViewController alloc];
    if( [deptTextfield.text length] == 0 ||
       [cityTextfield.text length] == 0 ||
       [jobnameTextFieldReturn.text length] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"Please fill in all the fields, before searching for jobs."
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
        
    }else
    {
        helperMethods *myInstance = [[helperMethods alloc] init];
        [myInstance saveDataSearch:self.jobnameTextFieldReturn.text :self.deptTextfield.text :self.cityTextfield.text]; 
        searchController.dept = deptTextfield.text;
        searchController.city = cityTextfield.text;
        searchController.keywords = @"keywords"; // need to change this
        searchController.jobname = self.jobnameTextFieldReturn.text;
        [self.navigationController pushViewController:searchController animated:YES];   
    }

}
@end
